<?php

namespace Ekoxe\SimpleOrders\Application\Suppliers;

use Ekoxe\SimpleOrders\Domain\Suppliers\SupplierFactory;
use Ekoxe\SimpleOrders\Domain\Suppliers\SupplierRepository;

class SupplierService
{
    private $supplierFactory;
    private $supplierRepository;

    public function __construct(SupplierFactory $supplierFactory, SupplierRepository $supplierRepository)
    {
        $this->supplierFactory = $supplierFactory;
        $this->supplierRepository = $supplierRepository;
    }

    public function listAll()
    {
        return $this->supplierRepository->findAll();
    }

    public function findByName($supplierName)
    {
        return $this->supplierRepository->findByName($supplierName);
    }

    public function register($supplierName, $supplierEmail)
    {
        $supplier = $this->supplierFactory->create($supplierName, $supplierEmail);
        $this->supplierRepository->persist($supplier);
        return $supplier;
    }

    public function remove($supplierName)
    {
        $this->supplierRepository->remove($supplierName);
    }

    public function changeEmail($supplierName, $newSupplierEmail)
    {
        $this->supplierRepository->update($supplierName, $newSupplierEmail);
    }
}
