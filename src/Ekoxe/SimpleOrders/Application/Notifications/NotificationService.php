<?php

namespace Ekoxe\SimpleOrders\Application\Notifications;

use Doctrine\Instantiator\Exception\InvalidArgumentException;
use Ekoxe\SimpleOrders\Domain\Orders\Order;
use Ekoxe\SimpleOrders\Domain\Suppliers\Supplier;

class NotificationService
{
    private $notificationDeliverer;

    public function __construct(NotificationDeliverer $notificationDeliverer)
    {
        $this->notificationDeliverer = $notificationDeliverer;
    }

    public function notifyAbout(Supplier $supplier, Order $order)
    {
        try {
            $this->notificationDeliverer->prepareNotification($supplier, $order);

            $this->notificationDeliverer->deliverNotification();
        } catch (InvalidArgumentException $e) {
        }
    }
}
