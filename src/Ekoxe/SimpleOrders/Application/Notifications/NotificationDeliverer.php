<?php


namespace Ekoxe\SimpleOrders\Application\Notifications;


use Ekoxe\SimpleOrders\Domain\Orders\Order;
use Ekoxe\SimpleOrders\Domain\Suppliers\Supplier;

interface NotificationDeliverer
{
    public function prepareNotification(Supplier $supplier, Order $order);

    public function deliverNotification();
}