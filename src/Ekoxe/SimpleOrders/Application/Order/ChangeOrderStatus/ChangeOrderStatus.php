<?php

namespace Ekoxe\SimpleOrders\Application\Order\ChangeOrderStatus;

use Ekoxe\SimpleOrders\Domain\Orders\OrderNumber;
use Ekoxe\SimpleOrders\Domain\Orders\OrderRepository;

class ChangeOrderStatus
{
    private $orderRepository;

    public function __construct(OrderRepository $orderRepository)
    {
        $this->orderRepository = $orderRepository;
    }

    public function handle(ChangeOrderStatusRequest $request)
    {
        $orderNumber = new OrderNumber($request->orderNumber);
        $orderStatus = $request->orderStatus;
        $order = $this->orderRepository->findByNumber($orderNumber);

        $order->changeStatus($orderStatus);

        $this->orderRepository->update($order);
    }
}
