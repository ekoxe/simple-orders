<?php


namespace Ekoxe\SimpleOrders\Application\Order\ChangeOrderStatus;


class ChangeOrderStatusRequest
{
    public $orderNumber;
    public $orderStatus;

    public function __construct($orderNumber, $orderStatus)
    {
        $this->orderNumber = $orderNumber;
        $this->orderStatus = $orderStatus;
    }
}