<?php

namespace Ekoxe\SimpleOrders\Application\Orders;

use Ekoxe\SimpleOrders\Domain\Customers\Customer;
use Ekoxe\SimpleOrders\Domain\Orders\Order;
use Ekoxe\SimpleOrders\Domain\Orders\OrderNumber;
use Ekoxe\SimpleOrders\Domain\Orders\OrderRepository;

class OrdersService
{
    private $orderRepository;

    public function __construct(OrderRepository $orderRepository)
    {
        $this->orderRepository = $orderRepository;
    }

    public function listAllOrders() : array
    {
        return $this->orderRepository->findAll();
    }

    public function listOrdersOfCustomer(Customer $customer) : array
    {
        return $this->orderRepository->findByCustomer($customer);
    }

    public function findOrderByNumber(OrderNumber $orderNumber): Order
    {
        return $this->orderRepository->findByNumber($orderNumber);
    }
}
