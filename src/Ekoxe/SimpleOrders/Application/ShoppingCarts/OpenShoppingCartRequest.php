<?php

namespace Ekoxe\SimpleOrders\Application\ShoppingCarts;

class OpenShoppingCartRequest
{
    public $customer;

    public function __construct($customerId)
    {
        $this->customer = $customerId;
    }
}