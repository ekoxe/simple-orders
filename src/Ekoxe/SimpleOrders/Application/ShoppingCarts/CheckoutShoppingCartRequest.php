<?php


namespace Ekoxe\SimpleOrders\Application\ShoppingCarts;


class CheckoutShoppingCartRequest
{
    public $shoppingCartNumber;
    public $supplierName;
    public $recipientName;
    public $recipientEmail;

    public function __construct($shoppingCartNumber, $supplierName, $recipientName, $recipientEmail)
    {
        $this->shoppingCartNumber = $shoppingCartNumber;
        $this->supplierName = $supplierName;
        $this->recipientName = $recipientName;
        $this->recipientEmail = $recipientEmail;
    }
}