<?php


namespace Ekoxe\SimpleOrders\Application\ShoppingCarts;


class AddProductIntoShoppingCartResponse
{
    public $success;

    public function __construct(bool $success)
    {
        $this->success = $success;
    }
}