<?php

namespace Ekoxe\SimpleOrders\Application\ShoppingCarts;

use Ekoxe\SimpleOrders\Domain\ShoppingCarts\CouldNotFindShoppingCartException;
use Ekoxe\SimpleOrders\Domain\ShoppingCarts\ShoppingCartNumber;
use Ekoxe\SimpleOrders\Domain\ShoppingCarts\ShoppingCartRepository;

class CloseShoppingCart
{
    private $shoppingCartRepository;

    public function __construct(ShoppingCartRepository $shoppingCartRepository)
    {
        $this->shoppingCartRepository = $shoppingCartRepository;
    }

    public function handle(CloseShoppingCartRequest $request) : CloseShoppingCartResponse
    {
        $shoppingCartNumber = new ShoppingCartNumber($request->shoppingCartNumber);

        try {
            $this->closeShoppingCartWithNumber($shoppingCartNumber);
            $success = true;
        } catch (CouldNotFindShoppingCartException $e) {
            $success = false;
        }

        return new CloseShoppingCartResponse($success);
    }

    private function closeShoppingCartWithNumber($shoppingCartNumber)
    {
        $shoppingCart = $this->shoppingCartRepository->findByNumber($shoppingCartNumber);

        $this->shoppingCartRepository->remove($shoppingCart);
    }
}
