<?php


namespace Ekoxe\SimpleOrders\Application\ShoppingCarts;

use Ekoxe\SimpleOrders\Domain\ShoppingCarts\ShoppingCart;

class OpenShoppingCartResponse
{
    public $shoppingCart;

    public function __construct(ShoppingCart $shoppingCart)
    {
        $this->shoppingCart = $shoppingCart;
    }
}