<?php

namespace Ekoxe\SimpleOrders\Application\ShoppingCarts;

use Ekoxe\SimpleOrders\Domain\ShoppingCarts\CouldNotFindShoppingCartException;
use Ekoxe\SimpleOrders\Domain\ShoppingCarts\Product;
use Ekoxe\SimpleOrders\Domain\ShoppingCarts\ProductFactory;
use Ekoxe\SimpleOrders\Domain\ShoppingCarts\ShoppingCartNumber;
use Ekoxe\SimpleOrders\Domain\ShoppingCarts\ShoppingCartRepository;

class AddProductIntoShoppingCart
{
    private $shoppingCartRepository;
    private $productFactory;

    public function __construct(ShoppingCartRepository $shoppingCartRepository, ProductFactory $productFactory)
    {
        $this->shoppingCartRepository = $shoppingCartRepository;
        $this->productFactory = $productFactory;
    }

    public function handle(AddProductIntoShoppingCartRequest $request) : AddProductIntoShoppingCartResponse
    {
        $shoppingCartNumber = new ShoppingCartNumber($request->shoppingCartNumber);
        $product = $this->productFactory->create($request->productReference, $request->productName, $request->quantity);

        try {
            $this->addProductIntoShoppingCart($product, $shoppingCartNumber);
            $success = true;
        } catch (CouldNotFindShoppingCartException $e) {
            $success = false;
        }

        return new AddProductIntoShoppingCartResponse($success);
    }

    private function addProductIntoShoppingCart(Product $product, ShoppingCartNumber $shoppingCartNumber)
    {
        $shoppingCart = $this->shoppingCartRepository->findByNumber($shoppingCartNumber);
        $shoppingCart->add($product);
        $this->shoppingCartRepository->update($shoppingCart);
    }
}
