<?php

namespace Ekoxe\SimpleOrders\Application\ShoppingCarts;

use Ekoxe\SimpleOrders\Domain\ShoppingCarts\CouldNotFindProductException;
use Ekoxe\SimpleOrders\Domain\ShoppingCarts\CouldNotFindShoppingCartException;
use Ekoxe\SimpleOrders\Domain\ShoppingCarts\ProductId;
use Ekoxe\SimpleOrders\Domain\ShoppingCarts\ProductReference;
use Ekoxe\SimpleOrders\Domain\ShoppingCarts\ShoppingCartNumber;
use Ekoxe\SimpleOrders\Domain\ShoppingCarts\ShoppingCartRepository;

class ChangeProductQuantityFromShoppingCart
{
    private $shoppingCartRepository;

    public function __construct(ShoppingCartRepository $shoppingCartRepository)
    {
        $this->shoppingCartRepository = $shoppingCartRepository;
    }

    public function handle(ChangeProductQuantityFromShoppingCartRequest $request) : ChangeProductQuantityFromShoppingCartResponse
    {
        $shoppingCartNumber = new ShoppingCartNumber($request->shoppingCartNumber);
        $productId = new ProductId(new ProductReference($request->productReference), $request->productName);

        try {
            $this->changeProductQuantity($shoppingCartNumber, $productId, $request->quantity);
            $success = true;
        } catch (CouldNotFindShoppingCartException $e) {
            $success = false;
        } catch (CouldNotFindProductException $e) {
            $success = false;
        }

        return new ChangeProductQuantityFromShoppingCartResponse($success);
    }

    private function changeProductQuantity(ShoppingCartNumber $shoppingCartNumber, ProductId $productId, $quantity)
    {
        $shoppingCart = $this->shoppingCartRepository->findByNumber($shoppingCartNumber);
        $shoppingCart->changeQuantityOf($productId, $quantity);
        $this->shoppingCartRepository->update($shoppingCart);
    }
}
