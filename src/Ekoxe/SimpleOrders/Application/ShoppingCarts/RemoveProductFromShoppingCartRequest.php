<?php


namespace Ekoxe\SimpleOrders\Application\ShoppingCarts;


class RemoveProductFromShoppingCartRequest
{
    public $shoppingCartNumber;
    public $productReference;
    public $productName;

    public function __construct($shoppingCartNumber, $productReference, $productName)
    {
        $this->shoppingCartNumber = $shoppingCartNumber;
        $this->productReference = $productReference;
        $this->productName = $productName;
    }
}