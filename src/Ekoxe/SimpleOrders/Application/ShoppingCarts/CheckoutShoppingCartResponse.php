<?php


namespace Ekoxe\SimpleOrders\Application\ShoppingCarts;


class CheckoutShoppingCartResponse
{
    public $success;

    public function __construct($success)
    {
        $this->success = $success;
    }

}