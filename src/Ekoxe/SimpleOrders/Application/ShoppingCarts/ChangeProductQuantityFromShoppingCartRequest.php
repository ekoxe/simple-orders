<?php

namespace Ekoxe\SimpleOrders\Application\ShoppingCarts;

class ChangeProductQuantityFromShoppingCartRequest
{
    public $shoppingCartNumber;
    public $productReference;
    public $productName;
    public $quantity;

    public function __construct($shoppingCartNumber, $productReference, $productName, $quantity)
    {
        $this->shoppingCartNumber = $shoppingCartNumber;
        $this->productReference = $productReference;
        $this->productName = $productName;
        $this->quantity = $quantity;
    }
}