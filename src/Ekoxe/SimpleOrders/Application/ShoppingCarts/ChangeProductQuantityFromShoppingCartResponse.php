<?php


namespace Ekoxe\SimpleOrders\Application\ShoppingCarts;


class ChangeProductQuantityFromShoppingCartResponse
{
    public $success;

    public function __construct($success)
    {
        $this->success = $success;
    }
}