<?php


namespace Ekoxe\SimpleOrders\Application\ShoppingCarts;


class CloseShoppingCartResponse
{
    public $success;

    public function __construct($success)
    {
        $this->success = $success;
    }
}