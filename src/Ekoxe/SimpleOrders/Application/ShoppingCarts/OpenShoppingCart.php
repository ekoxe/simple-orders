<?php

namespace Ekoxe\SimpleOrders\Application\ShoppingCarts;

use Ekoxe\SimpleOrders\Domain\ShoppingCarts\CouldNotFindShoppingCartException;
use Ekoxe\SimpleOrders\Domain\ShoppingCarts\ShoppingCart;
use Ekoxe\SimpleOrders\Domain\ShoppingCarts\ShoppingCartFactory;
use Ekoxe\SimpleOrders\Domain\ShoppingCarts\ShoppingCartRepository;

class OpenShoppingCart
{
    private $shoppingCartRepository;
    private $shoppingCartFactory;

    public function __construct(ShoppingCartRepository $shoppingCartRepository, ShoppingCartFactory $shoppingCartFactory)
    {
        $this->shoppingCartRepository = $shoppingCartRepository;
        $this->shoppingCartFactory = $shoppingCartFactory;
    }

    public function handle(OpenShoppingCartRequest $request) : OpenShoppingCartResponse
    {
        $shoppingCart = $this->openShoppingCart($request->customer);
        return new OpenShoppingCartResponse($shoppingCart);
    }

    private function openShoppingCart($customer) : ShoppingCart
    {
        try {
            return $this->openShoppingCartOfCustomer($customer);
        } catch (CouldNotFindShoppingCartException $e) {
            return $this->openNewShoppingCartForCustomer($customer);
        }
    }

    private function openShoppingCartOfCustomer($customer) : ShoppingCart
    {
        $shoppingCart = $this->shoppingCartRepository->findByCustomer($customer);
        return $shoppingCart;
    }

    private function openNewShoppingCartForCustomer($customerId) : ShoppingCart
    {
        $shoppingCart = $this->shoppingCartFactory->create($customerId);
        $this->shoppingCartRepository->persist($shoppingCart);
        return $shoppingCart;
    }
}