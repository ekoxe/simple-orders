<?php


namespace Ekoxe\SimpleOrders\Application\ShoppingCarts;


class CloseShoppingCartRequest
{
    public $shoppingCartNumber;

    public function __construct($shoppingCartNumber)
    {
        $this->shoppingCartNumber = $shoppingCartNumber;
    }
}