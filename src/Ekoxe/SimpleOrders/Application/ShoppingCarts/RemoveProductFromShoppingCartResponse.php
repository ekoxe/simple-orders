<?php


namespace Ekoxe\SimpleOrders\Application\ShoppingCarts;


class RemoveProductFromShoppingCartResponse
{
    public $success;

    public function __construct(bool $success)
    {
        $this->success = $success;
    }
}