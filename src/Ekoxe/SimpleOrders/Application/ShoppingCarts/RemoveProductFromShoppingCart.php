<?php

namespace Ekoxe\SimpleOrders\Application\ShoppingCarts;

use Ekoxe\SimpleOrders\Domain\ShoppingCarts\CouldNotFindProductException;
use Ekoxe\SimpleOrders\Domain\ShoppingCarts\CouldNotFindShoppingCartException;
use Ekoxe\SimpleOrders\Domain\ShoppingCarts\ProductId;
use Ekoxe\SimpleOrders\Domain\ShoppingCarts\ProductReference;
use Ekoxe\SimpleOrders\Domain\ShoppingCarts\ShoppingCartNumber;
use Ekoxe\SimpleOrders\Domain\ShoppingCarts\ShoppingCartRepository;

class RemoveProductFromShoppingCart
{
    private $shoppingCartRepository;

    public function __construct(ShoppingCartRepository $shoppingCartRepository)
    {
        $this->shoppingCartRepository = $shoppingCartRepository;
    }

    public function handle(RemoveProductFromShoppingCartRequest $request) : RemoveProductFromShoppingCartResponse
    {
        $productId = new ProductId(new ProductReference($request->productReference), $request->productName);
        $shoppingCartNumber = new ShoppingCartNumber($request->shoppingCartNumber);

        try {
            $this->removeProductFromShoppingCart($productId, $shoppingCartNumber);
            $success = true;
        } catch (CouldNotFindShoppingCartException $e) {
            $success = false;
        } catch (CouldNotFindProductException $e) {
            $success = false;
        }

        return new RemoveProductFromShoppingCartResponse($success);
    }

    private function removeProductFromShoppingCart(ProductId $productId, ShoppingCartNumber $shoppingCartNumber)
    {
        $shoppingCart = $this->shoppingCartRepository->findByNumber($shoppingCartNumber);
        $shoppingCart->remove($productId);
        $this->shoppingCartRepository->update($shoppingCart);
    }
}
