<?php

namespace Ekoxe\SimpleOrders\Application\ShoppingCarts;

use Ekoxe\SimpleOrders\Application\Notifications\NotificationService;
use Ekoxe\SimpleOrders\Domain\Orders\AlreadyPersistedOrderException;
use Ekoxe\SimpleOrders\Domain\Orders\OrderFactory;
use Ekoxe\SimpleOrders\Domain\Orders\OrderRepository;
use Ekoxe\SimpleOrders\Domain\Orders\Recipient;
use Ekoxe\SimpleOrders\Domain\ShoppingCarts\CouldNotFindShoppingCartException;
use Ekoxe\SimpleOrders\Domain\ShoppingCarts\ShoppingCartNumber;
use Ekoxe\SimpleOrders\Domain\ShoppingCarts\ShoppingCartRepository;
use Ekoxe\SimpleOrders\Domain\Suppliers\CouldNotFindSupplierException;
use Ekoxe\SimpleOrders\Domain\Suppliers\SupplierRepository;

class CheckoutShoppingCart
{
    private $shoppingCartRepository;

    private $orderFactory;
    private $orderRepository;

    private $supplierRepository;

    private $notificationService;

    public function __construct(ShoppingCartRepository $shoppingCartRepository, OrderFactory $orderFactory, OrderRepository $orderRepository, SupplierRepository $supplierRepository, NotificationService $notificationService)
    {
        $this->shoppingCartRepository = $shoppingCartRepository;
        $this->orderFactory = $orderFactory;
        $this->orderRepository = $orderRepository;
        $this->supplierRepository = $supplierRepository;
        $this->notificationService = $notificationService;
    }

    public function handle(CheckoutShoppingCartRequest $request) : CheckoutShoppingCartResponse
    {
        try {
            $shoppingCart = $this->shoppingCartRepository->findByNumber(new ShoppingCartNumber($request->shoppingCartNumber));
            $supplier = $this->supplierRepository->findByName($request->supplierName);
            $recipient = new Recipient($request->recipientName, $request->recipientEmail);

            $orderNumber = $this->orderRepository->nextOrderNumber();
            $order = $this->orderFactory->createForShoppingCart($shoppingCart, $orderNumber, $recipient);
            $this->orderRepository->persist($order);
            $this->shoppingCartRepository->remove($shoppingCart);

            $this->notificationService->notifyAbout($supplier, $order);
            $success = true;
        } catch (\InvalidArgumentException $e) {
            $success = false;
        } catch (CouldNotFindShoppingCartException $e) {
            $success = false;
        } catch (CouldNotFindSupplierException $e) {
            $success = false;
        } catch (AlreadyPersistedOrderException $e) {
            $success = false;
        }

        return new CheckoutShoppingCartResponse($success);
    }
}
