<?php


namespace Ekoxe\SimpleOrders\Domain\Suppliers;

use Exception;

class AlreadyPersistedSupplierException extends Exception
{
    public static function withName($supplierName) : AlreadyPersistedSupplierException
    {
        return new static(sprintf('A supplier with name "%s" was already persisted', $supplierName));
    }
}