<?php


namespace Ekoxe\SimpleOrders\Domain\Suppliers;


interface SupplierRepository
{
    public function persist(Supplier $supplier);

    public function findByName($supplierName) : Supplier;

    public function findAll() : array;

    public function update($supplierName, $newEmail);

    public function remove($supplierName);
}
