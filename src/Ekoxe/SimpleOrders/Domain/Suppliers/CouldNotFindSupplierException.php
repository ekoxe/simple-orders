<?php


namespace Ekoxe\SimpleOrders\Domain\Suppliers;


class CouldNotFindSupplierException extends \Exception
{
    public function __construct($message = '')
    {
        parent::__construct($message);
    }

    public static function withName($supplierName) : CouldNotFindSupplierException
    {
        return new static(sprintf('Could not find supplier with name "%s"', $supplierName));
    }
}