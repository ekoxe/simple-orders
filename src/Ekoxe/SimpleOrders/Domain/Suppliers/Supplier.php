<?php


namespace Ekoxe\SimpleOrders\Domain\Suppliers;


class Supplier
{
    private $contactInformation;

    public function __construct(ContactInformation $contactInformation)
    {
        $this->contactInformation = $contactInformation;
    }

    public function equals(Supplier $supplier) : bool
    {
        return $this->contactInformation->getName() === $supplier->contactInformation->getName();
    }

    public function getContactInformation()
    {
        return $this->contactInformation;
    }
}