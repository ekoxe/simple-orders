<?php


namespace Ekoxe\SimpleOrders\Domain\Suppliers;


interface SupplierFactory
{
    public function create($name, $email) : Supplier;
}