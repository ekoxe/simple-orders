<?php


namespace Ekoxe\SimpleOrders\Domain\Orders;

use Ekoxe\DDDUtil\Domain\AggregateRoot;
use Ekoxe\SimpleOrders\Domain\Customers\Customer;

class Order extends AggregateRoot
{
    protected $number;
    protected $customer;
    protected $recipient;
    protected $items;
    protected $status;
    protected $placedAt;

    public function __construct(OrderNumber $number, Customer $customer, Recipient $recipient, array $items = [], $status = OrderStatus::PENDING, \DateTimeImmutable $placedAt = null)
    {
        $this->customer = $customer;
        $this->recipient = $recipient;
        $this->items = $items;
        $this->number = $number;
        $this->status = $status;
        $this->placedAt = is_null($placedAt) ? new \DateTimeImmutable() : $placedAt;
    }

    public function hasNumber(OrderNumber $number): bool
    {
        return $this->number()->isSameAs($number);
    }

    public function number(): OrderNumber
    {
        return $this->number;
    }

    public function hasCustomer(Customer $customer): bool
    {
        return $this->customer->equals($customer);
    }

    public function getCustomer(): Customer
    {
        return $this->customer;
    }

    public function hasRecipient(Recipient $recipient): bool
    {
        return $this->recipient->equals($recipient);
    }

    public function getRecipient(): Recipient
    {
        return $this->recipient;
    }

    public function getItems()
    {
        return $this->items;
    }

    public function contains(LineItem $item): bool
    {
        /** @var LineItem $orderItem */
        foreach ($this->items as $orderItem) {
            if ($orderItem->equals($item)) {
                return true;
            }
        }

        return false;
    }

    public function status(): string
    {
        return $this->status;
    }

    public function hasStatus($status): bool
    {
        return (string)$this->status === (string)$status;
    }

    public function changeStatus($status)
    {
        if (!OrderStatus::isValid($status)) {
            throw InvalidOrderStatusException::withValue($status);
        }

        $this->recordApplyAndPublishThat(new OrderStatusWasChanged($this->number(), $status));
    }

    public function placedAt(): \DateTimeImmutable
    {
        return $this->placedAt;
    }

    protected function applyOrderStatusWasChanged(OrderStatusWasChanged $event)
    {
        $this->status = $event->orderStatus();
    }
}