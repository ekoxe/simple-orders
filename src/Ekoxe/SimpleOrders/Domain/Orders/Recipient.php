<?php


namespace Ekoxe\SimpleOrders\Domain\Orders;


class Recipient
{
    private $name;
    private $email;

    public function __construct($name, $email)
    {
        $this->name = $name;
        $this->email = $email;
    }

    public function equals(Recipient $recipient) : bool
    {
        return $this->name === $recipient->name
        && $this->email === $recipient->email;
    }

    public function getName()
    {
        return (string)$this->name;
    }

    public function getEmail()
    {
        return (string)$this->email;
    }
}