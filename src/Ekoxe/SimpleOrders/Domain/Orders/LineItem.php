<?php


namespace Ekoxe\SimpleOrders\Domain\Orders;


class LineItem
{
    private $quantity;
    private $reference;
    private $name;

    public function __construct($quantity, $reference, $name)
    {
        $this->quantity = $quantity;
        $this->reference = $reference;
        $this->name = $name;
    }

    public function equals(LineItem $item) : bool
    {
        return $this->quantity === $item->quantity
        && $this->name === $item->name
        && $this->reference === $item->reference;
    }

    public function getQuantity()
    {
        return (string)$this->quantity;
    }

    public function getReference()
    {
        return (string)$this->reference;
    }

    public function getName()
    {
        return (string)$this->name;
    }
}