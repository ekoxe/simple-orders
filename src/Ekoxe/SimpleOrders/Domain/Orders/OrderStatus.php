<?php


namespace Ekoxe\SimpleOrders\Domain\Orders;


class OrderStatus
{
    const PENDING = 'pending';
    const PROCESSING = 'processing';
    const SHIPPED = 'shipped';

    public static function isValid($status): bool
    {
        $constants = (new \ReflectionClass(OrderStatus::class))->getConstants();
        return in_array($status, $constants);
    }
}