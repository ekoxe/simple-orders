<?php


namespace Ekoxe\SimpleOrders\Domain\Orders;


use Ekoxe\SimpleOrders\Domain\ShoppingCarts\ShoppingCart;

interface OrderFactory
{
    public function createForShoppingCart(ShoppingCart $shoppingCart, OrderNumber $number, Recipient $recipient): Order;
}