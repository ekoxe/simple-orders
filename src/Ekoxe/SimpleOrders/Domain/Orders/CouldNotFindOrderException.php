<?php


namespace Ekoxe\SimpleOrders\Domain\Orders;


class CouldNotFindOrderException extends \Exception
{
    public static function withNumber(OrderNumber $number): CouldNotFindOrderException
    {
        return new static(sprintf('Could not find order with number "%s".', (string)$number));
    }
}