<?php


namespace Ekoxe\SimpleOrders\Domain\Orders;


class OrderNumber
{
    private $number;

    public function __construct($number)
    {
        $this->number = $number;
    }

    public function isSameAs(OrderNumber $other): bool
    {
        return (string)$this->number === (string)$other->number;
    }

    public function __toString(): string
    {
        return (string)$this->number;
    }
}