<?php


namespace Ekoxe\SimpleOrders\Domain\Orders;


use Ekoxe\DDDUtil\Domain\DomainEvent;

class OrderStatusWasChanged extends DomainEvent
{
    private $orderNumber;
    private $orderStatus;

    public function __construct(OrderNumber $orderNumber, $orderStatus)
    {
        $occurredOn = new \DateTimeImmutable();
        parent::__construct($occurredOn);

        $this->orderNumber = $orderNumber;
        $this->orderStatus = $orderStatus;
    }

    public function orderNumber(): OrderNumber
    {
        return $this->orderNumber;
    }

    public function orderStatus(): string
    {
        return $this->orderStatus;
    }
}