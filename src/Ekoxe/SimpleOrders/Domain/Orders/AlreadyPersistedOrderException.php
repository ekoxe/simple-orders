<?php


namespace Ekoxe\SimpleOrders\Domain\Orders;


use Exception;

class AlreadyPersistedOrderException extends \Exception
{
    public function __construct($message = '')
    {
        parent::__construct($message);
    }

    public static function withNumber(OrderNumber $number) : AlreadyPersistedOrderException
    {
        return new static(sprintf('An order with number "%s" is already persisted', (string)$number));
    }
}