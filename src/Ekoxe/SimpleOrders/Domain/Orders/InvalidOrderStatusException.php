<?php


namespace Ekoxe\SimpleOrders\Domain\Orders;


class InvalidOrderStatusException extends \InvalidArgumentException
{
    public static function withValue($status): InvalidOrderStatusException
    {
        return new static(sprintf('"%s" is not a valid order status.', (string)$status));
    }
}