<?php


namespace Ekoxe\SimpleOrders\Domain\Orders;

use Ekoxe\SimpleOrders\Domain\Customers\Customer;

interface OrderRepository
{
    public function nextOrderNumber(): OrderNumber;

    public function findAll() : array;

    public function findByNumber(OrderNumber $number): Order;

    public function findByCustomer(Customer $customer): array;

    public function persist(Order $order);

    public function update(Order $order);
}
