<?php


namespace Ekoxe\SimpleOrders\Domain\Customers;


class CustomerId
{
    private $value;

    public function __construct($value)
    {
        $this->value = $value;
    }

    public function equals(CustomerId $customerId) : bool
    {
        return $this->value === $customerId->value;
    }

    public function __toString(): string
    {
        return (string)$this->value;
    }
}