<?php


namespace Ekoxe\SimpleOrders\Domain\Customers;


class Customer
{
    private $id;

    public function __construct(CustomerId $id)
    {
        $this->id = $id;
    }

    public function equals(Customer $customer) : bool
    {
        return $this->id->equals($customer->id);
    }

    public function __toString(): string
    {
        return (string)$this->id;
    }
}