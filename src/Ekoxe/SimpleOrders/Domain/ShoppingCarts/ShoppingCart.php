<?php


namespace Ekoxe\SimpleOrders\Domain\ShoppingCarts;

use Ekoxe\SimpleOrders\Domain\Customers\Customer;

class ShoppingCart
{
    protected $number;
    protected $customer;
    protected $products;

    public function __construct(ShoppingCartNumber $number, Customer $customer, array $products = [])
    {
        $this->number = $number;
        $this->customer = $customer;
        $this->products = $products;
    }

    function add(Product $product)
    {
        if ($this->contains($product)) {
            $this->mergeProductQuantities($product);
        } else {
            $this->products[] = $product;
        }
    }

    function contains(Product $product) : bool
    {
        /** @var Product $addedProduct */
        foreach ($this->products as $addedProduct) {
            if ($addedProduct->equals($product)) {
                return true;
            }
        }
        return false;
    }

    private function mergeProductQuantities(Product $product)
    {
        /** @var Product $addedProducts */
        foreach ($this->products as $addedProducts) {
            if ($addedProducts->equals($product)) {
                $addedProducts->mergeWith($product);
            }
        }
    }

    function remove(ProductId $productId)
    {
        /** @var Product $product */
        foreach ($this->products as $key => $product) {
            if ($product->has($productId)) {
                unset($this->products[$key]);
                return;
            }
        }

        throw CouldNotFindProductException::inShoppingCart($this->number, $productId);
    }

    public function changeQuantityOf(ProductId $productId, $quantity)
    {
        $product = $this->getProduct($productId);
        $product->setQuantity($quantity);
    }

    private function getProduct(ProductId $productId)
    {
        /** @var Product $product */
        foreach ($this->products as $product) {
            if ($product->has($productId)) {
                return $product;
            }
        }

        throw CouldNotFindProductException::inShoppingCart($this->number, $productId);
    }

    public function getCustomer(): Customer
    {
        return $this->customer;
    }

    public function getProducts(): array
    {
        return $this->products;
    }

    public function equals(ShoppingCart $shoppingCart)
    {
        return $this->number->equals($shoppingCart->number);
    }
}