<?php


namespace Ekoxe\SimpleOrders\Domain\ShoppingCarts;

use Ramsey\Uuid\Uuid;

class ShoppingCartNumber
{
    private $value;

    public function __construct($value)
    {
        $this->value = $value;
    }

    public static function generate() : ShoppingCartNumber
    {
        return new static((string)Uuid::uuid4());
    }

    public function equals(ShoppingCartNumber $number)
    {
        return (string)$this->value === (string)$number->value;
    }

    function __toString()
    {
        return (string)$this->value;
    }
}