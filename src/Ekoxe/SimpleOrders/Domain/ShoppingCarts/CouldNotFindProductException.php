<?php


namespace Ekoxe\SimpleOrders\Domain\ShoppingCarts;


class CouldNotFindProductException extends \Exception
{
    public function __construct($message = "")
    {
        parent::__construct($message);
    }

    public static function inShoppingCart(ShoppingCartNumber $shoppingCartNumber, ProductId $productId) : CouldNotFindProductException
    {
        return new static(sprintf('Shopping cart "%s" does not contain product "%s"', (string)$shoppingCartNumber, (string)$productId));
    }
}