<?php

namespace Ekoxe\SimpleOrders\Domain\ShoppingCarts;

interface ProductFactory
{
    function create($reference, $name, $quantity) : Product;
}