<?php

namespace Ekoxe\SimpleOrders\Domain\ShoppingCarts;

interface ShoppingCartRepository
{
    function findByNumber(ShoppingCartNumber $shoppingCartNumber) : ShoppingCart;

    function findByCustomer($customer) : ShoppingCart;

    function persist(ShoppingCart $shoppingCart);

    function update(ShoppingCart $shoppingCart);

    function remove(ShoppingCart $shoppingCart);
}
