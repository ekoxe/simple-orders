<?php


namespace Ekoxe\SimpleOrders\Domain\ShoppingCarts;


class ProductReference
{
    private $value;

    public function __construct($value)
    {
        $this->value = $value;
    }

    public function equals(ProductReference $reference) : bool
    {
        return $this->value === $reference->value;
    }

    function __toString()
    {
        return (string)$this->value;
    }
}