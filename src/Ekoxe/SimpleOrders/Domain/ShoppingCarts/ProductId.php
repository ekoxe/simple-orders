<?php


namespace Ekoxe\SimpleOrders\Domain\ShoppingCarts;


class ProductId
{
    private $reference;
    private $name;

    public function __construct(ProductReference $reference, $name)
    {
        $this->reference = $reference;
        $this->name = $name;
    }

    public function equals(ProductId $productId) : bool
    {
        return $this->reference->equals($productId->reference) && $this->name === $productId->name;
    }

    function __toString()
    {
        return (string)$this->reference . ' - ' . (string)$this->name;
    }
}