<?php


namespace Ekoxe\SimpleOrders\Domain\ShoppingCarts;


interface ShoppingCartFactory
{
    public function create($customer) : ShoppingCart;

    public function rehydrate($shoppingCartNumber, $customer, array $products = []) : ShoppingCart;
}