<?php

namespace Ekoxe\SimpleOrders\Domain\ShoppingCarts;

use Exception;

class CouldNotFindShoppingCartException extends \Exception
{
    public function __construct($message = "")
    {
        parent::__construct($message);
    }

    public static function forCustomer($customerId) : CouldNotFindShoppingCartException
    {
        return new static(sprintf('Customer with id "%s" does not have any shopping cart', $customerId));
    }

    public static function withNumber(ShoppingCartNumber $shoppingCartNumber) : CouldNotFindShoppingCartException
    {
        return new static(sprintf('Shopping cart with number "%s" does not exist', (string)$shoppingCartNumber));
    }
}