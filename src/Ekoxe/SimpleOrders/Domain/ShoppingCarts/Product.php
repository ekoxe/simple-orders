<?php


namespace Ekoxe\SimpleOrders\Domain\ShoppingCarts;


class Product
{
    protected $reference;
    protected $name;
    protected $quantity;
    private $identity;

    public function __construct(ProductReference $reference, $name, $quantity)
    {
        $this->identity = new ProductId($reference, $name);
        $this->reference = $reference;
        $this->name = $name;
        $this->quantity = $quantity;
    }

    public function has(ProductId $productId) : bool
    {
        return $this->identity->equals($productId);
    }

    public function mergeWith(Product $product)
    {
        if ($this->equals($product)) {
            $this->quantity += $product->quantity;
        }
    }

    public function equals(Product $product) : bool
    {
        return $this->reference->equals($product->reference) && $this->name === $product->name;
    }

    public function getReference()
    {
        return $this->reference;
    }

    public function getName()
    {
        return $this->name;
    }

    public function getQuantity()
    {
        return $this->quantity;
    }

    public function setQuantity($quantity)
    {
        $this->quantity = $quantity;
    }
}