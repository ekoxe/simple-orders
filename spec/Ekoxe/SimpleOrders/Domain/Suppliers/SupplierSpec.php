<?php

namespace spec\Ekoxe\SimpleOrders\Domain\Suppliers;

use Ekoxe\SimpleOrders\Domain\Suppliers\ContactInformation;
use Ekoxe\SimpleOrders\Domain\Suppliers\Supplier;
use PhpSpec\ObjectBehavior;
use Prophecy\Argument;

class SupplierSpec extends ObjectBehavior
{
    const A_NAME = 'Hahah';
    const ANOTHER_NAME = 'NEIN';
    const AN_EMAIL = 'pretty@joke.com';

    function let()
    {
        $this->beConstructedWith(new ContactInformation(self::A_NAME, self::AN_EMAIL));
    }

    function it_equals_another_supplier_with_same_name()
    {
        $supplier = new Supplier(new ContactInformation(self::A_NAME, self::AN_EMAIL));

        $this->equals($supplier)->shouldBe(true);
    }

    function it_does_not_equal_another_supplier_with_different_name()
    {
        $supplier = new Supplier(new ContactInformation(self::ANOTHER_NAME, self::AN_EMAIL));

        $this->equals($supplier)->shouldBe(false);
    }
}
