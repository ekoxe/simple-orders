<?php

namespace spec\Ekoxe\SimpleOrders\Domain\Customers;

use Ekoxe\SimpleOrders\Domain\Customers\CustomerId;
use PhpSpec\ObjectBehavior;

class CustomerIdSpec extends ObjectBehavior
{

    const A_CUSTOMER_ID = 'a customer id';
    const ANOTHER_CUSTOMER_ID = 'another customer id';

    function let()
    {
        $this->beConstructedWith(self::A_CUSTOMER_ID);
    }

    function it_equals_another_customer_id_with_same_value()
    {
        $customerId = new CustomerId(self::A_CUSTOMER_ID);

        $this->equals($customerId)->shouldBe(true);
    }

    function it_does_not_equal_another_customer_id_with_different_value()
    {
        $customerId = new CustomerId(self::ANOTHER_CUSTOMER_ID);

        $this->equals($customerId)->shouldBe(false);
    }

    function it_formats_to_string()
    {
        $this->__toString()->shouldBe(self::A_CUSTOMER_ID);
    }
}
