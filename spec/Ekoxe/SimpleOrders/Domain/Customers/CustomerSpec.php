<?php

namespace spec\Ekoxe\SimpleOrders\Domain\Customers;

use Ekoxe\SimpleOrders\Domain\Customers\Customer;
use Ekoxe\SimpleOrders\Domain\Customers\CustomerId;
use PhpSpec\ObjectBehavior;

class CustomerSpec extends ObjectBehavior
{
    const A_CUSTOMER_ID = 'yo customer';
    const ANOTHER_CUSTOMER_ID = 'hell no';

    private $customerId;

    function let()
    {
        $this->customerId = new CustomerId(self::A_CUSTOMER_ID);
        $this->beConstructedWith($this->customerId);
    }

    function it_equals_another_customer_with_same_id()
    {
        $customer = new Customer($this->customerId);
        $this->equals($customer)->shouldBe(true);
    }

    function it_does_not_equal_another_customer_with_different_id()
    {
        $customer = new Customer(new CustomerId(self::ANOTHER_CUSTOMER_ID));

        $this->equals($customer)->shouldBe(false);
    }

    function it_formats_to_string()
    {
        $this->__toString()->shouldBe(self::A_CUSTOMER_ID);
    }
}
