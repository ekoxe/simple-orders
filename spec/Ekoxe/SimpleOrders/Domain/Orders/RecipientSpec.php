<?php

namespace spec\Ekoxe\SimpleOrders\Domain\Orders;

use Ekoxe\SimpleOrders\Domain\Orders\Recipient;
use PhpSpec\ObjectBehavior;
use Prophecy\Argument;

class RecipientSpec extends ObjectBehavior
{
    const A_NAME = 'Hay';
    const ANOTHER_NAME = 'Yop';

    const AN_EMAIL = 'hay@sunshine.com';
    const ANOTHER_EMAIL = 'yop@sunshine.com';

    function let()
    {
        $this->beConstructedWith(self::A_NAME, self::AN_EMAIL);
    }

    function it_equals_another_recipient_with_same_values()
    {
        $this->equals(new Recipient(self::A_NAME, self::AN_EMAIL));
    }

    function it_does_not_equal_another_recipient_with_different_name()
    {
        $this->equals(new Recipient(self::ANOTHER_NAME, self::AN_EMAIL));
    }

    function it_does_not_equal_another_recipient_with_different_email()
    {
        $this->equals(new Recipient(self::A_NAME, self::ANOTHER_EMAIL));
    }
}
