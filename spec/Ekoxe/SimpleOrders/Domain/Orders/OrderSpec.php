<?php

namespace spec\Ekoxe\SimpleOrders\Domain\Orders;

use Ekoxe\DDDUtil\Domain\DomainEventPublisher;
use Ekoxe\DDDUtil\Helper\SpySubscriber;
use Ekoxe\SimpleOrders\Domain\Customers\Customer;
use Ekoxe\SimpleOrders\Domain\Customers\CustomerId;
use Ekoxe\SimpleOrders\Domain\Orders\InvalidOrderStatusException;
use Ekoxe\SimpleOrders\Domain\Orders\LineItem;
use Ekoxe\SimpleOrders\Domain\Orders\OrderNumber;
use Ekoxe\SimpleOrders\Domain\Orders\OrderStatus;
use Ekoxe\SimpleOrders\Domain\Orders\OrderStatusWasChanged;
use Ekoxe\SimpleOrders\Domain\Orders\Recipient;
use PhpSpec\ObjectBehavior;

class OrderSpec extends ObjectBehavior
{
    const A_CUSTOMER = 'YAH';
    const ANOTHER_CUSTOMER = 'Nah';

    const A_RECIPIENT_NAME = 'Ghetto man';
    const A_RECIPIENT_EMAIL = 'yo@ghetto.com';
    const ANOTHER_RECIPIENT_NAME = 'Acme Corp';
    const ANOTHER_RECIPIENT_EMAIL = 'derp@acmecorp.com';

    const A_PRODUCT_QUANTITY = 61;
    const A_PRODUCT_REFERENCE = 'KSAFHOPGB';
    const A_PRODUCT_NAME = 'Eagle';

    const ANOTHER_PRODUCT_QUANTITY = 741;
    const ANOTHER_PRODUCT_REFERENCE = '7412JHHSD';
    const ANOTHER_PRODUCT_NAME = 'Go Pro Hero';
    const AN_ORDER_NUMBER = 13574;

    private $number;
    private $customer;

    private $anotherItem;
    private $item;

    private $recipient;
    private $anotherRecipient;

    function let()
    {
        $this->number = new OrderNumber(self::AN_ORDER_NUMBER);
        $this->customer = new Customer(new CustomerId(self::A_CUSTOMER));
        $this->recipient = new Recipient(self::A_RECIPIENT_NAME, self::A_RECIPIENT_EMAIL);
        $this->anotherRecipient = new Recipient(self::ANOTHER_RECIPIENT_NAME, self::ANOTHER_RECIPIENT_EMAIL);
        $this->item = new LineItem(self::A_PRODUCT_QUANTITY, self::A_PRODUCT_REFERENCE, self::A_PRODUCT_NAME);
        $this->anotherItem = new LineItem(self::ANOTHER_PRODUCT_QUANTITY, self::ANOTHER_PRODUCT_REFERENCE, self::ANOTHER_PRODUCT_NAME);

        $this->beConstructedWith($this->number, $this->customer, $this->recipient, [$this->item]);
    }

    function it_has_number_given_same_order_number()
    {
        $this->shouldHaveNumber($this->number);
    }

    function it_does_not_have_number_number_given_different_order_number()
    {
        $number = new OrderNumber(999);

        $this->shouldNotHaveNumber($number);
    }

    function it_has_customer_given_same_customer()
    {
        $this->shouldHaveCustomer($this->customer);
    }

    function it_does_not_customer_given_different_customer()
    {
        $customer = new Customer(new CustomerId(self::ANOTHER_CUSTOMER));

        $this->shouldNotHaveCustomer($customer);
    }

    function it_has_recipient_given_same_recipient()
    {
        $this->shouldHaveRecipient($this->recipient);
    }

    function it_does_not_have_recipient_given_different_recipient()
    {
        $this->shouldNotHaveRecipient($this->anotherRecipient);
    }

    function it_contains_item_given_item_was_added_to_order()
    {
        $this->contains($this->item)->shouldBe(true);
    }

    function it_does_not_contain_item_given_item_was_not_added_to_order()
    {
        $this->contains($this->anotherItem)->shouldBe(false);
    }

    function it_is_pending_by_default()
    {
        $this->shouldHaveStatus(OrderStatus::PENDING);
    }

    function it_has_status_given_same_status()
    {
        $this->shouldHaveStatus(OrderStatus::PENDING);
    }

    function it_does_not_have_status_given_different_status()
    {
        $this->shouldNotHaveStatus(OrderStatus::SHIPPED);
    }

    function it_changes_status()
    {
        $this->changeStatus(OrderStatus::SHIPPED);

        $this->shouldHaveStatus(OrderStatus::SHIPPED);
    }

    function it_publishes_order_status_was_changed_event_given_status_was_changed()
    {
        $newStatus = OrderStatus::PROCESSING;
        $subscriber = new SpySubscriber();
        DomainEventPublisher::instance()->subscribe($subscriber);

        $this->changeStatus($newStatus);
        DomainEventPublisher::instance()->unsubscribe($subscriber);

        $this->verifyOrderStatusWasChanged($subscriber, $newStatus);
    }

    private function verifyOrderStatusWasChanged($subscriber, $newStatus)
    {
        $event = $subscriber->receivedEvent;
        assert($event instanceof OrderStatusWasChanged);
        $this->number()->shouldBeSameAs($event->orderNumber());
        assert($newStatus === $event->orderStatus());
    }

    function it_throws_when_changing_status_given_status_invalid()
    {
        $invalidOrderStatus = 'invalid_status';

        $this
            ->shouldThrow(InvalidOrderStatusException::withValue($invalidOrderStatus))
            ->duringChangeStatus($invalidOrderStatus);
    }
}
