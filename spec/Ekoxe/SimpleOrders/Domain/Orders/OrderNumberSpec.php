<?php

namespace spec\Ekoxe\SimpleOrders\Domain\Orders;

use Ekoxe\SimpleOrders\Domain\Orders\OrderNumber;
use PhpSpec\ObjectBehavior;

class OrderNumberSpec extends ObjectBehavior
{
    private $number = 74536;

    function let()
    {
        $this->beConstructedWith($this->number);
    }

    function it_equals_another_order_number_given_same_number()
    {
        $orderNumber = new OrderNumber($this->number);

        $this->shouldBeSameAs($orderNumber);
    }

    function it_does_not_equal_another_order_number_given_different_number()
    {
        $orderNumber = new OrderNumber(657);

        $this->shouldNotBeSameAs($orderNumber);
    }

    function it_formats_to_string()
    {
        $this->__toString()->shouldBe((string)$this->number);
    }
}
