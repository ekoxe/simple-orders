<?php

namespace spec\Ekoxe\SimpleOrders\Domain\Orders;

use Ekoxe\SimpleOrders\Domain\Orders\OrderStatus;
use PhpSpec\ObjectBehavior;

class OrderStatusSpec extends ObjectBehavior
{
    function it_is_valid_given_order_status_pending_processing_or_shipped()
    {
        $this->shouldBeValid(OrderStatus::PENDING);
        $this->shouldBeValid(OrderStatus::PROCESSING);
        $this->shouldBeValid(OrderStatus::SHIPPED);
    }

    function it_is_not_valid_given_order_status_not_pending_processing_or_shipped()
    {
        $this->shouldNotBeValid('invalid_status');
    }
}
