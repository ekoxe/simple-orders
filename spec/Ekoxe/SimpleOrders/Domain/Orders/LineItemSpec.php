<?php

namespace spec\Ekoxe\SimpleOrders\Domain\Orders;

use Ekoxe\SimpleOrders\Domain\Orders\LineItem;
use PhpSpec\ObjectBehavior;
use Prophecy\Argument;

class LineItemSpec extends ObjectBehavior
{
    const A_QUANTITY = 9;
    const ANOTHER_QUANTITY = 3;

    const A_REFERENCE = 'ASFIUH';
    const ANOTHER_REFERENCE = '83YYU55';

    const A_NAME = 'That name bro';
    const ANOTHER_NAME = 'You kiddin?';

    function let()
    {
        $this->beConstructedWith(self::A_QUANTITY, self::A_REFERENCE, self::A_NAME);
    }

    function it_equals_another_line_item_with_same_values()
    {
        $this->equals(new LineItem(self::A_QUANTITY, self::A_REFERENCE, self::A_NAME))->shouldBe(true);
    }

    function it_does_not_equal_another_line_item_with_different_quantity()
    {
        $this->equals(new LineItem(self::ANOTHER_QUANTITY, self::A_REFERENCE, self::A_NAME))->shouldBe(false);
    }

    function it_does_not_equal_another_line_item_with_different_reference()
    {
        $this->equals(new LineItem(self::A_QUANTITY, self::ANOTHER_REFERENCE, self::A_NAME))->shouldBe(false);
    }

    function it_does_not_equal_another_line_item_with_different_name()
    {
        $this->equals(new LineItem(self::A_QUANTITY, self::A_REFERENCE, self::ANOTHER_NAME))->shouldBe(false);
    }
}
