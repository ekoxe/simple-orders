<?php

namespace spec\Ekoxe\SimpleOrders\Domain\ShoppingCarts;

use Ekoxe\SimpleOrders\Domain\Customers\Customer;
use Ekoxe\SimpleOrders\Domain\Customers\CustomerId;
use Ekoxe\SimpleOrders\Domain\ShoppingCarts\CouldNotFindProductException;
use Ekoxe\SimpleOrders\Domain\ShoppingCarts\Product;
use Ekoxe\SimpleOrders\Domain\ShoppingCarts\ProductId;
use Ekoxe\SimpleOrders\Domain\ShoppingCarts\ProductReference;
use Ekoxe\SimpleOrders\Domain\ShoppingCarts\ShoppingCart;
use Ekoxe\SimpleOrders\Domain\ShoppingCarts\ShoppingCartNumber;
use PhpSpec\ObjectBehavior;

class ShoppingCartSpec extends ObjectBehavior
{
    const A_SHOPPING_CART_NUMBER = 7741;
    const ANOTHER_SHOPPING_CART_NUMBER = 79;

    const A_CUSTOMER_ID = 'acme';

    const A_PRODUCT_REFERENCE = 543;
    const ANOTHER_PRODUCT_REFERENCE = 917;
    const A_PRODUCT_NAME = 'potato';
    const A_PRODUCT_QUANTITY = 1;
    const ANOTHER_PRODUCT_QUANTITY = 4;

    private $shoppingCartNumber;
    private $customer;

    function let()
    {
        $this->shoppingCartNumber = new ShoppingCartNumber(self::A_SHOPPING_CART_NUMBER);
        $this->customer = new Customer(new CustomerId(self::A_CUSTOMER_ID));

        $this->beConstructedWith($this->shoppingCartNumber, $this->customer);
    }

    function it_contains_product_given_product_was_added_into_shopping_cart()
    {
        $product = new Product(new ProductReference(self::A_PRODUCT_REFERENCE), self::A_PRODUCT_NAME, self::A_PRODUCT_QUANTITY);
        $this->add($product);

        $this->contains($product)->shouldBe(true);
    }

    function it_does_not_contain_product_given_product_was_not_added_into_shopping_cart()
    {
        $product = new Product(new ProductReference(self::A_PRODUCT_REFERENCE), self::A_PRODUCT_NAME, self::A_PRODUCT_QUANTITY);

        $this->contains($product)->shouldBe(false);
    }

    function it_adds_product_into_shopping_cart_given_product_was_not_into_shopping()
    {
        $product = new Product(new ProductReference(self::A_PRODUCT_REFERENCE), self::A_PRODUCT_NAME, self::A_PRODUCT_QUANTITY);

        $this->add($product);

        $this->contains($product)->shouldBe(true);
    }

    function it_merges_products_given_product_was_already_into_shopping_cart(Product $aProduct, Product $anotherProduct)
    {
        $this->prepareShoppingCartWithProducts($aProduct, $anotherProduct);

        $aProduct->mergeWith($aProduct)->shouldBeCalled();

        $this->add($aProduct);
    }

    private function prepareShoppingCartWithProducts(Product $aProduct, Product $anotherProduct)
    {
        $aProduct->equals($aProduct)->willReturn(true);
        $aProduct->equals($anotherProduct)->willReturn(false);
        $anotherProduct->equals($aProduct)->willReturn(false);
        $this->add($aProduct);
        $this->add($anotherProduct);
    }

    function it_removes_product_from_shopping_cart()
    {
        $productReference = new ProductReference(self::A_PRODUCT_REFERENCE);
        $productId = new ProductId($productReference, self::A_PRODUCT_NAME);
        $product = new Product($productReference, self::A_PRODUCT_NAME, self::A_PRODUCT_QUANTITY);
        $this->add($product);

        $this->remove($productId);

        $this->contains($product)->shouldBe(false);
    }

    function it_throws_when_removing_product_given_product_not_into_shopping_cart()
    {
        $productId = new ProductId(new ProductReference(self::A_PRODUCT_REFERENCE), self::A_PRODUCT_NAME);
        $this->shouldThrow(CouldNotFindProductException::class)->duringRemove($productId);
    }

    function it_changes_quantity_of_a_product(Product $aProduct, Product $anotherProduct)
    {
        $productId = new ProductId(new ProductReference(self::A_PRODUCT_REFERENCE), self::A_PRODUCT_NAME);
        $aProduct->equals($anotherProduct)->willReturn(false);
        $aProduct->has($productId)->willReturn(true);
        $anotherProduct->has($productId)->willReturn(false);
        $this->add($aProduct);
        $this->add($anotherProduct);

        $aProduct->setQuantity(self::ANOTHER_PRODUCT_QUANTITY)->shouldBeCalled();
        $anotherProduct->setQuantity(self::ANOTHER_PRODUCT_QUANTITY)->shouldNotBeCalled();

        $this->changeQuantityOf($productId, self::ANOTHER_PRODUCT_QUANTITY);
    }

    function it_throws_when_changing_quantity_of_a_product_given_product_not_into_shopping_cart()
    {
        $productId = new ProductId(new ProductReference(self::A_PRODUCT_REFERENCE), self::A_PRODUCT_NAME);
        $this->shouldThrow(CouldNotFindProductException::class)->duringChangeQuantityOf($productId, self::ANOTHER_PRODUCT_QUANTITY);
    }

    function it_equals_a_shopping_cart_with_same_number()
    {
        $shoppingCart = new ShoppingCart($this->shoppingCartNumber, $this->customer);

        $this->equals($shoppingCart)->shouldBe(true);
    }

    function it_does_not_equal_a_shopping_cart_with_different_number()
    {
        $shoppingCart = new ShoppingCart(new ShoppingCartNumber(self::ANOTHER_SHOPPING_CART_NUMBER), $this->customer);

        $this->equals($shoppingCart)->shouldBe(false);
    }
}
