<?php

namespace spec\Ekoxe\SimpleOrders\Domain\ShoppingCarts;

use Ekoxe\SimpleOrders\Domain\ShoppingCarts\ProductId;
use Ekoxe\SimpleOrders\Domain\ShoppingCarts\ProductReference;
use PhpSpec\ObjectBehavior;
use Prophecy\Argument;

class ProductIdSpec extends ObjectBehavior
{
    const A_REFERENCE = 'FA85F';
    const A_NAME = 'rum';
    const ANOTHER_REFERENCE = 'VK221X';
    const ANOTHER_NAME = 'vodka';

    function let()
    {
        $this->beConstructedWith(new ProductReference(self::A_REFERENCE), self::A_NAME);
    }

    function it_equals_another_product_id_with_same_values()
    {
        $productId = new ProductId(new ProductReference(self::A_REFERENCE), self::A_NAME);

        $this->equals($productId)->shouldBe(true);
    }

    function it_does_not_equal_another_product_id_with_different_reference()
    {
        $productId = new ProductId(new ProductReference(self::ANOTHER_REFERENCE), self::A_NAME);

        $this->equals($productId)->shouldBe(false);
    }

    function it_does_not_equal_another_product_id_with_different_name()
    {
        $productId = new ProductId(new ProductReference(self::A_REFERENCE), self::ANOTHER_NAME);

        $this->equals($productId)->shouldBe(false);
    }
}
