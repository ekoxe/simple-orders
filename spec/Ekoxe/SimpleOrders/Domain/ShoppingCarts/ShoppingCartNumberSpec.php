<?php

namespace spec\Ekoxe\SimpleOrders\Domain\ShoppingCarts;

use Ekoxe\SimpleOrders\Domain\ShoppingCarts\ShoppingCartNumber;
use PhpSpec\ObjectBehavior;
use Prophecy\Argument;
use Ramsey\Uuid\Uuid;

class ShoppingCartNumberSpec extends ObjectBehavior
{
    private $aValue;

    function let()
    {
        $this->aValue = (string)Uuid::uuid4();
        $this->beConstructedWith($this->aValue);
    }

    function it_generates_a_shopping_cart_number()
    {
        $this->generate()->__toString()->shouldNotBeEmpty();
    }

    function it_equals_a_shopping_cart_number_with_same_value()
    {
        $shoppingCartNumber = new ShoppingCartNumber($this->aValue);

        $this->equals($shoppingCartNumber)->shouldBe(true);
    }

    function it_does_not_equal_a_shopping_cart_number_with_different_value()
    {
        $shoppingCartNumber = new ShoppingCartNumber((string)Uuid::uuid4());

        $this->equals($shoppingCartNumber)->shouldBe(false);
    }

    function getMatchers()
    {
        return [
            'beEmpty' => function ($element) {
                return empty($element);
            }
        ];
    }
}
