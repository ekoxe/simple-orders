<?php

namespace spec\Ekoxe\SimpleOrders\Domain\ShoppingCarts;

use Ekoxe\SimpleOrders\Domain\ShoppingCarts\Product;
use Ekoxe\SimpleOrders\Domain\ShoppingCarts\ProductId;
use Ekoxe\SimpleOrders\Domain\ShoppingCarts\ProductReference;
use PhpSpec\ObjectBehavior;
use Prophecy\Argument;

class ProductSpec extends ObjectBehavior
{
    const A_PRODUCT_REFERENCE = 795;
    const ANOTHER_PRODUCT_REFERENCE = 158;

    const A_QUANTITY = 3;
    const A_NAME = 'ice cream';
    const ANOTHER_NAME = 'potato';
    const ANOTHER_QUANTITY = 1;

    function let()
    {
        $this->beConstructedWith(new ProductReference(self::A_PRODUCT_REFERENCE), self::A_NAME, self::A_QUANTITY);
    }

    function it_has_the_identity_given_same_product_id()
    {
        $productId = new ProductId(new ProductReference(self::A_PRODUCT_REFERENCE), self::A_NAME);

        $this->has($productId)->shouldBe(true);
    }

    function it_does_not_have_the_identity_given_different_product_id()
    {
        $productId = new ProductId(new ProductReference(self::ANOTHER_PRODUCT_REFERENCE), self::A_NAME);

        $this->has($productId)->shouldBe(false);
    }

    function it_adds_quantity_when_merging_with_a_product_given_same_product()
    {
        $product = new Product(new ProductReference(self::A_PRODUCT_REFERENCE), self::A_NAME, self::ANOTHER_QUANTITY);

        $this->mergeWith($product);

        $expectedQuantity = self::A_QUANTITY + self::ANOTHER_QUANTITY;
        $this->getQuantity()->shouldBe($expectedQuantity);
    }

    function it_does_not_add_quantity_when_merging_with_a_product_given_different_product()
    {
        $product = new Product(new ProductReference(self::ANOTHER_PRODUCT_REFERENCE), self::A_NAME, self::ANOTHER_QUANTITY);

        $this->mergeWith($product);

        $this->getQuantity()->shouldBe(self::A_QUANTITY);
    }

    function it_equals_another_product_with_same_product_reference_and_same_name()
    {
        $product = new Product(new ProductReference(self::A_PRODUCT_REFERENCE), self::A_NAME, self::A_QUANTITY);

        $this->equals($product)->shouldBe(true);
    }

    function it_does_not_equal_another_product_with_different_product_reference()
    {
        $product = new Product(new ProductReference(self::ANOTHER_PRODUCT_REFERENCE), self::A_NAME, self::A_QUANTITY);

        $this->equals($product)->shouldBe(false);
    }

    function it_does_not_equal_another_product_with_different_name()
    {
        $product = new Product(new ProductReference(self::A_PRODUCT_REFERENCE), self::ANOTHER_NAME, self::A_QUANTITY);

        $this->equals($product)->shouldBe(false);
    }
}
