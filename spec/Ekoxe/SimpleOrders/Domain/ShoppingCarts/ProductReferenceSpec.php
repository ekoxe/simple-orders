<?php

namespace spec\Ekoxe\SimpleOrders\Domain\ShoppingCarts;

use Ekoxe\SimpleOrders\Domain\ShoppingCarts\ProductReference;
use PhpSpec\ObjectBehavior;
use Prophecy\Argument;

class ProductReferenceSpec extends ObjectBehavior
{
    const A_PRODUCT_REFERENCE = 795;
    const ANOTHER_PRODUCT_REFERENCE = 158;

    function it_equals_another_product_reference_with_same_value()
    {
        $this->beConstructedWith(self::A_PRODUCT_REFERENCE);
        $reference = new ProductReference(self::A_PRODUCT_REFERENCE);

        $this->equals($reference)->shouldBe(true);
    }

    function it_does_not_equal_another_product_reference_with_different_value()
    {
        $this->beConstructedWith(self::A_PRODUCT_REFERENCE);
        $reference = new ProductReference(self::ANOTHER_PRODUCT_REFERENCE);

        $this->equals($reference)->shouldBe(false);
    }
}
