<?php

namespace spec\Ekoxe\SimpleOrders\Application\Order\ChangeOrderStatus;

use Ekoxe\DDDUtil\Domain\DomainEventPublisher;
use Ekoxe\DDDUtil\Helper\SpySubscriber;
use Ekoxe\SimpleOrders\Application\Order\ChangeOrderStatus\ChangeOrderStatusRequest;
use Ekoxe\SimpleOrders\Domain\Customers\Customer;
use Ekoxe\SimpleOrders\Domain\Customers\CustomerId;
use Ekoxe\SimpleOrders\Domain\Orders\CouldNotFindOrderException;
use Ekoxe\SimpleOrders\Domain\Orders\InvalidOrderStatusException;
use Ekoxe\SimpleOrders\Domain\Orders\Order;
use Ekoxe\SimpleOrders\Domain\Orders\OrderNumber;
use Ekoxe\SimpleOrders\Domain\Orders\OrderRepository;
use Ekoxe\SimpleOrders\Domain\Orders\OrderStatus;
use Ekoxe\SimpleOrders\Domain\Orders\OrderStatusWasChanged;
use Ekoxe\SimpleOrders\Domain\Orders\Recipient;
use PhpSpec\ObjectBehavior;

class ChangeOrderStatusSpec extends ObjectBehavior
{
    function let(OrderRepository $orderRepository)
    {
        $this->beConstructedWith($orderRepository);
    }

    function it_changes_an_order_status($orderRepository)
    {
        $order = $this->createOrder();
        $request = $this->prepareChangeOrderStatusRequest($orderRepository, $order);
        $subscriber = new SpySubscriber();
        DomainEventPublisher::instance()->subscribe($subscriber);

        $this->handle($request);
        DomainEventPublisher::instance()->unsubscribe($subscriber);

        $this->verifyOrderStatusWasChanged($subscriber, $order, $request);
    }

    function it_updates_the_order_given_its_status_was_changed($orderRepository)
    {
        $order = $this->createOrder();
        $request = $this->prepareChangeOrderStatusRequest($orderRepository, $order);

        $orderRepository->update($order)->shouldBeCalled();

        $this->handle($request);
    }

    function it_throws_given_order_not_found($orderRepository)
    {
        $request = $this->prepareNotFoundOrderRequest($orderRepository);

        $this
            ->shouldThrow(CouldNotFindOrderException::class)
            ->duringHandle($request);
    }

    function it_throws_given_order_status_invalid($orderRepository)
    {
        $order = $this->createOrder();
        $request = $this->prepareInvalidRequest($orderRepository, $order);

        $this
            ->shouldThrow(InvalidOrderStatusException::withValue($request->orderStatus))
            ->duringHandle($request);
    }

    private function prepareChangeOrderStatusRequest(OrderRepository $orderRepository, Order $order): ChangeOrderStatusRequest
    {
        $orderRepository->findByNumber($order->number())->willReturn($order);
        $orderRepository->update($order)->shouldBeCalled();
        return new ChangeOrderStatusRequest((string)$order->number(), OrderStatus::PROCESSING);
    }

    private function createOrder(): Order
    {
        $orderNumber = new OrderNumber(2963);
        $customer = new Customer(new CustomerId('Ekoxe'));
        $recipient = new Recipient('Ian', 'letourneau.ian@gmail.com');
        return new Order($orderNumber, $customer, $recipient);
    }

    private function verifyOrderStatusWasChanged($subscriber, $order, $request)
    {
        $event = $subscriber->receivedEvent;
        assert($event instanceof OrderStatusWasChanged);
        assert($event->orderNumber()->isSameAs($order->number()));
        assert($event->orderStatus() === $request->orderStatus);
    }

    private function prepareNotFoundOrderRequest(OrderRepository $orderRepository)
    {
        $orderNumber = new OrderNumber(48);
        $orderRepository->findByNumber($orderNumber)->willThrow(CouldNotFindOrderException::withNumber($orderNumber));
        return new ChangeOrderStatusRequest((string)$orderNumber, OrderStatus::SHIPPED);
    }

    private function prepareInvalidRequest(OrderRepository $orderRepository, Order $order)
    {
        $orderRepository->findByNumber($order->number())->willReturn($order);
        return new ChangeOrderStatusRequest((string)$order->number(), 'invalid_order_status');
    }
}
