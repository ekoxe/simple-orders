<?php

namespace spec\Ekoxe\SimpleOrders\Application\Orders;

use Ekoxe\SimpleOrders\Domain\Customers\Customer;
use Ekoxe\SimpleOrders\Domain\Orders\Order;
use Ekoxe\SimpleOrders\Domain\Orders\OrderNumber;
use Ekoxe\SimpleOrders\Domain\Orders\OrderRepository;
use PhpSpec\ObjectBehavior;

class OrdersServiceSpec extends ObjectBehavior
{
    function let(OrderRepository $orderRepository)
    {
        $this->beConstructedWith($orderRepository);
    }

    function it_lists_all_orders(Order $anOrder, Order $anotherOrder, OrderRepository $orderRepository)
    {
        $orders = [$anOrder, $anotherOrder];
        $orderRepository->findAll()->willReturn($orders);

        $this->listAllOrders()->shouldContainExactlyElementsIn($orders);
    }

    function it_lists_orders_of_a_customer(Order $anOrder, Order $anotherOrder, Customer $customer, OrderRepository $orderRepository)
    {
        $orders = [$anOrder, $anotherOrder];
        $orderRepository->findByCustomer($customer)->willReturn($orders);

        $this->listOrdersOfCustomer($customer)->shouldContainExactlyElementsIn($orders);
    }

    function it_finds_order_by_number(OrderRepository $orderRepository, OrderNumber $orderNumber, Order $order)
    {
        $orderRepository->findByNumber($orderNumber)->willReturn($order);

        $this->findOrderByNumber($orderNumber)->shouldBe($order);
    }

    function getMatchers()
    {
        return [
            'containExactlyElementsIn' => function ($subjects, $elements) {
                if (count($subjects) !== count($elements)) {
                    return false;
                }

                foreach ($elements as $element) {
                    if (!in_array($element, $subjects)) {
                        return false;
                    }
                }

                foreach ($subjects as $subject) {
                    if (!in_array($subject, $elements)) {
                        return false;
                    }
                }

                return true;
            }
        ];
    }
}
