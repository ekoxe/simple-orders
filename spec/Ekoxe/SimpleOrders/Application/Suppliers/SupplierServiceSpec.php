<?php

namespace spec\Ekoxe\SimpleOrders\Application\Suppliers;

use Ekoxe\SimpleOrders\Domain\Suppliers\AlreadyPersistedSupplierException;
use Ekoxe\SimpleOrders\Domain\Suppliers\ContactInformation;
use Ekoxe\SimpleOrders\Domain\Suppliers\CouldNotFindSupplierException;
use Ekoxe\SimpleOrders\Domain\Suppliers\Supplier;
use Ekoxe\SimpleOrders\Domain\Suppliers\SupplierFactory;
use Ekoxe\SimpleOrders\Domain\Suppliers\SupplierRepository;
use PhpSpec\ObjectBehavior;
use Prophecy\Argument;

class SupplierServiceSpec extends ObjectBehavior
{
    const A_SUPPLIER_NAME = 'Formamed Sarl';
    const A_SUPPLIER_EMAIL = 'commandes@formamed.ch';
    const ANOTHER_SUPPLIER_NAME = 'Acme Corp';
    const ANOTHER_SUPPLIER_EMAIL = 'acme@corp.com';

    function let(SupplierFactory $supplierFactory, SupplierRepository $supplierRepository)
    {
        $this->beConstructedWith($supplierFactory, $supplierRepository);
    }

    function it_lists_all_suppliers(SupplierRepository $supplierRepository)
    {
        $suppliers = [
            new Supplier(new ContactInformation(self::A_SUPPLIER_NAME, self::A_SUPPLIER_EMAIL)),
            new Supplier(new ContactInformation(self::ANOTHER_SUPPLIER_NAME, self::ANOTHER_SUPPLIER_EMAIL)),
        ];
        $supplierRepository->findAll()->willReturn($suppliers);

        $this->listAll()->shouldReturn($suppliers);
    }

    function it_finds_supplier_by_name(SupplierRepository $supplierRepository)
    {
        $supplier = new Supplier(new ContactInformation(self::A_SUPPLIER_NAME, self::A_SUPPLIER_EMAIL));
        $supplierRepository->findByName(self::A_SUPPLIER_NAME)->willReturn($supplier);

        $this->findByName(self::A_SUPPLIER_NAME)->shouldReturn($supplier);
    }

    function it_throws_when_finding_supplier_by_name_given_supplier_could_not_be_found(SupplierRepository $supplierRepository)
    {
        $supplierRepository->findByName(self::A_SUPPLIER_NAME)->willThrow(CouldNotFindSupplierException::class);

        $this->shouldThrow(CouldNotFindSupplierException::class)->duringFindByName(self::A_SUPPLIER_NAME);
    }

    function it_registers_a_supplier(SupplierFactory $supplierFactory, SupplierRepository $supplierRepository)
    {
        $supplier = new Supplier(new ContactInformation(self::A_SUPPLIER_NAME, self::A_SUPPLIER_EMAIL));
        $supplierFactory->create(self::A_SUPPLIER_NAME, self::A_SUPPLIER_EMAIL)->willReturn($supplier);

        $this->register(self::A_SUPPLIER_NAME, self::A_SUPPLIER_EMAIL)->shouldReturn($supplier);
        $supplierRepository->persist($supplier)->shouldHaveBeenCalled();
    }

    function it_throws_when_registering_a_supplier_given_supplier_already_persisted(SupplierFactory $supplierFactory, SupplierRepository $supplierRepository)
    {
        $supplier = new Supplier(new ContactInformation(self::A_SUPPLIER_NAME, self::A_SUPPLIER_EMAIL));
        $supplierFactory->create(self::A_SUPPLIER_NAME, self::A_SUPPLIER_EMAIL)->willReturn($supplier);
        $supplierRepository->persist($supplier)->willThrow(AlreadyPersistedSupplierException::class);

        $this->shouldThrow(AlreadyPersistedSupplierException::class)->duringRegister(self::A_SUPPLIER_NAME, self::A_SUPPLIER_EMAIL);
    }

    function it_changes_supplier_email(SupplierRepository $supplierRepository)
    {
        $this->changeEmail(self::A_SUPPLIER_NAME, self::ANOTHER_SUPPLIER_EMAIL);

        $supplierRepository->update(self::A_SUPPLIER_NAME, self::ANOTHER_SUPPLIER_EMAIL)->shouldHaveBeenCalled();
    }

    function it_throws_when_changing_supplier_email_given_supplier_could_not_be_found(SupplierRepository $supplierRepository)
    {
        $supplierRepository->update(self::A_SUPPLIER_NAME, self::ANOTHER_SUPPLIER_EMAIL)->willThrow(CouldNotFindSupplierException::class);

        $this->shouldThrow(CouldNotFindSupplierException::class)->duringChangeEmail(self::A_SUPPLIER_NAME, self::ANOTHER_SUPPLIER_EMAIL);
    }

    function it_removes_a_supplier(SupplierRepository $supplierRepository)
    {
        $this->remove(self::A_SUPPLIER_NAME);

        $supplierRepository->remove(self::A_SUPPLIER_NAME)->shouldHaveBeenCalled();
    }

    function it_throws_when_removing_a_supplier_given_supplier_could_not_be_found(SupplierRepository $supplierRepository)
    {
        $supplierRepository->remove(self::A_SUPPLIER_NAME)->willThrow(CouldNotFindSupplierException::class);

        $this->shouldThrow(CouldNotFindSupplierException::class)->duringRemove(self::A_SUPPLIER_NAME);
    }
}
