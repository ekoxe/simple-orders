<?php

namespace spec\Ekoxe\SimpleOrders\Application\ShoppingCarts;

use Ekoxe\SimpleOrders\Application\Notifications\NotificationService;
use Ekoxe\SimpleOrders\Application\ShoppingCarts\CheckoutShoppingCartRequest;
use Ekoxe\SimpleOrders\Domain\Orders\AlreadyPersistedOrderException;
use Ekoxe\SimpleOrders\Domain\Orders\Order;
use Ekoxe\SimpleOrders\Domain\Orders\OrderFactory;
use Ekoxe\SimpleOrders\Domain\Orders\OrderNumber;
use Ekoxe\SimpleOrders\Domain\Orders\OrderRepository;
use Ekoxe\SimpleOrders\Domain\Orders\Recipient;
use Ekoxe\SimpleOrders\Domain\ShoppingCarts\CouldNotFindShoppingCartException;
use Ekoxe\SimpleOrders\Domain\ShoppingCarts\ShoppingCart;
use Ekoxe\SimpleOrders\Domain\ShoppingCarts\ShoppingCartRepository;
use Ekoxe\SimpleOrders\Domain\Suppliers\CouldNotFindSupplierException;
use Ekoxe\SimpleOrders\Domain\Suppliers\Supplier;
use Ekoxe\SimpleOrders\Domain\Suppliers\SupplierRepository;
use InvalidArgumentException;
use PhpSpec\ObjectBehavior;
use Prophecy\Argument;
use Ramsey\Uuid\Uuid;

class CheckoutShoppingCartSpec extends ObjectBehavior
{
    const A_SUPPLIER_NAME = 'Formamed';
    const A_RECIPIENT_NAME = 'Ian Letourneau';
    const A_RECIPIENT_EMAIL = 'info@swican.com';

    private $request;

    private $recipient;
    private $orderNumber;

    function let(ShoppingCartRepository $shoppingCartRepository, OrderFactory $orderFactory, OrderRepository $orderRepository, SupplierRepository $supplierRepository, NotificationService $notificationService)
    {
        $this->request = new CheckoutShoppingCartRequest((string)Uuid::uuid4(), self::A_SUPPLIER_NAME, self::A_RECIPIENT_NAME, self::A_RECIPIENT_EMAIL);
        $this->recipient = new Recipient(self::A_RECIPIENT_NAME, self::A_RECIPIENT_EMAIL);
        $this->orderNumber = new OrderNumber(58795);

        $orderRepository->nextOrderNumber()->willReturn($this->orderNumber);

        $this->beConstructedWith($shoppingCartRepository, $orderFactory, $orderRepository, $supplierRepository, $notificationService);
    }

    function it_creates_an_order_from_shopping_cart(ShoppingCartRepository $shoppingCartRepository, ShoppingCart $shoppingCart, OrderFactory $orderFactory, OrderRepository $orderRepository, Order $order, SupplierRepository $supplierRepository, Supplier $supplier)
    {
        $shoppingCartRepository->findByNumber($this->request->shoppingCartNumber)->willReturn($shoppingCart);
        $supplierRepository->findByName($this->request->supplierName)->willReturn($supplier);
        $orderFactory->createForShoppingCart($shoppingCart, $this->orderNumber, $this->recipient)->willReturn($order);

        $shoppingCartRepository->remove($shoppingCart)->shouldBeCalled();
        $orderRepository->persist($order)->shouldBeCalled();

        $this->handle($this->request);
    }

    function it_does_not_create_an_order_given_it_could_not_find_shopping_cart(ShoppingCartRepository $shoppingCartRepository, OrderFactory $orderFactory, OrderRepository $orderRepository)
    {
        $shoppingCartRepository->findByNumber($this->request->shoppingCartNumber)->willThrow(CouldNotFindShoppingCartException::class);

        $this->handle($this->request);

        $orderFactory->createForShoppingCart(Argument::any(), Argument::any(), Argument::any())->shouldNotHaveBeenCalled();
        $orderRepository->persist(Argument::any())->shouldNotHaveBeenCalled();
    }

    function it_does_not_create_an_order_given_it_could_not_find_supplier(ShoppingCartRepository $shoppingCartRepository, ShoppingCart $shoppingCart, OrderFactory $orderFactory, OrderRepository $orderRepository, SupplierRepository $supplierRepository)
    {
        $shoppingCartRepository->findByNumber($this->request->shoppingCartNumber)->willReturn($shoppingCart);
        $supplierRepository->findByName($this->request->supplierName)->willThrow(CouldNotFindSupplierException::class);

        $this->handle($this->request);

        $orderFactory->createForShoppingCart(Argument::any(), Argument::any(), Argument::any())->shouldNotHaveBeenCalled();
        $orderRepository->persist(Argument::any())->shouldNotHaveBeenCalled();
    }

    function it_does_not_create_an_order_given_empty_shopping_cart(ShoppingCartRepository $shoppingCartRepository, ShoppingCart $shoppingCart, OrderRepository $orderRepository, OrderFactory $orderFactory, Order $order, SupplierRepository $supplierRepository, Supplier $supplier)
    {
        $shoppingCartRepository->findByNumber($this->request->shoppingCartNumber)->willReturn($shoppingCart);
        $supplierRepository->findByName($this->request->supplierName)->willReturn($supplier);
        $orderFactory->createForShoppingCart($shoppingCart, $this->orderNumber, $this->recipient)->willReturn($order)->willThrow(InvalidArgumentException::class);

        $this->handle($this->request);

        $orderRepository->persist(Argument::any())->shouldNotHaveBeenCalled();
    }

    function it_removes_the_shopping_cart_given_the_order_was_created(ShoppingCartRepository $shoppingCartRepository, ShoppingCart $shoppingCart, OrderFactory $orderFactory, Order $order, SupplierRepository $supplierRepository, Supplier $supplier, $orderRepository)
    {
        $shoppingCartRepository->findByNumber($this->request->shoppingCartNumber)->willReturn($shoppingCart);
        $supplierRepository->findByName($this->request->supplierName)->willReturn($supplier);
        $orderFactory->createForShoppingCart($shoppingCart, $this->orderNumber, $this->recipient)->willReturn($order);
        $orderRepository->persist($order)->shouldBeCalled();

        $shoppingCartRepository->remove($shoppingCart)->shouldBeCalled();

        $this->handle($this->request);
    }

    function it_does_not_remove_shopping_cart_given_it_could_not_find_shopping_cart(ShoppingCartRepository $shoppingCartRepository)
    {
        $shoppingCartRepository->findByNumber($this->request->shoppingCartNumber)->willThrow(CouldNotFindShoppingCartException::class);

        $this->handle($this->request);

        $shoppingCartRepository->remove(Argument::any())->shouldNotHaveBeenCalled();
    }

    function it_does_not_remove_shopping_cart_given_it_could_not_find_supplier(ShoppingCartRepository $shoppingCartRepository, ShoppingCart $shoppingCart, SupplierRepository $supplierRepository)
    {
        $shoppingCartRepository->findByNumber($this->request->shoppingCartNumber)->willReturn($shoppingCart);
        $supplierRepository->findByName($this->request->supplierName)->willThrow(CouldNotFindSupplierException::class);

        $this->handle($this->request);

        $shoppingCartRepository->remove(Argument::any())->shouldNotHaveBeenCalled();
    }

    function it_does_not_remove_shopping_cart_given_empty_shopping_cart(ShoppingCartRepository $shoppingCartRepository, ShoppingCart $shoppingCart, OrderRepository $orderRepository, OrderFactory $orderFactory, Order $order, SupplierRepository $supplierRepository, Supplier $supplier)
    {
        $shoppingCartRepository->findByNumber($this->request->shoppingCartNumber)->willReturn($shoppingCart);
        $supplierRepository->findByName($this->request->supplierName)->willReturn($supplier);
        $orderFactory->createForShoppingCart($shoppingCart, $this->orderNumber, $this->recipient)->willReturn($order)->willThrow(InvalidArgumentException::class);

        $this->handle($this->request);

        $shoppingCartRepository->remove(Argument::any())->shouldNotHaveBeenCalled();
    }

    function it_does_not_remove_shopping_cart_given_order_already_persisted(ShoppingCartRepository $shoppingCartRepository, ShoppingCart $shoppingCart, OrderRepository $orderRepository, OrderFactory $orderFactory, Order $order, SupplierRepository $supplierRepository, Supplier $supplier)
    {
        $shoppingCartRepository->findByNumber($this->request->shoppingCartNumber)->willReturn($shoppingCart);
        $supplierRepository->findByName($this->request->supplierName)->willReturn($supplier);
        $orderFactory->createForShoppingCart($shoppingCart, $this->orderNumber, $this->recipient)->willReturn($order);
        $orderRepository->persist($order)->willThrow(AlreadyPersistedOrderException::class);

        $this->handle($this->request);

        $shoppingCartRepository->remove(Argument::any())->shouldNotHaveBeenCalled();
    }

    function it_notifies_the_supplier_about_the_order(ShoppingCartRepository $shoppingCartRepository, ShoppingCart $shoppingCart, OrderFactory $orderFactory, Order $order, NotificationService $notificationService, SupplierRepository $supplierRepository, Supplier $supplier, $orderRepository)
    {
        $supplierRepository->findByName($this->request->supplierName)->willReturn($supplier);
        $shoppingCartRepository->findByNumber($this->request->shoppingCartNumber)->willReturn($shoppingCart);
        $orderFactory->createForShoppingCart($shoppingCart, $this->orderNumber, $this->recipient)->willReturn($order);
        $shoppingCartRepository->remove($shoppingCart)->shouldBeCalled();
        $orderRepository->persist($order)->shouldBeCalled();

        $this->handle($this->request);

        $notificationService->notifyAbout($supplier, $order)->shouldHaveBeenCalled();
    }

    function it_does_not_notify_the_supplier_about_the_order_given_it_could_not_find_shopping_cart(ShoppingCartRepository $shoppingCartRepository, NotificationService $notificationService)
    {
        $shoppingCartRepository->findByNumber($this->request->shoppingCartNumber)->willThrow(CouldNotFindShoppingCartException::class);

        $this->handle($this->request);

        $notificationService->notifyAbout(Argument::any(), Argument::any())->shouldNotHaveBeenCalled();
    }

    function it_does_not_notify_the_supplier_about_the_order_given_it_could_not_find_supplier(ShoppingCartRepository $shoppingCartRepository, ShoppingCart $shoppingCart, NotificationService $notificationService, SupplierRepository $supplierRepository)
    {
        $shoppingCartRepository->findByNumber($this->request->shoppingCartNumber)->willReturn($shoppingCart);
        $supplierRepository->findByName($this->request->supplierName)->willThrow(CouldNotFindSupplierException::class);

        $this->handle($this->request);

        $notificationService->notifyAbout(Argument::any(), Argument::any())->shouldNotHaveBeenCalled();
    }

    function it_does_not_notify_supplier_given_empty_shopping_cart(ShoppingCartRepository $shoppingCartRepository, ShoppingCart $shoppingCart, OrderRepository $orderRepository, OrderFactory $orderFactory, Order $order, SupplierRepository $supplierRepository, Supplier $supplier, NotificationService $notificationService)
    {
        $shoppingCartRepository->findByNumber($this->request->shoppingCartNumber)->willReturn($shoppingCart);
        $supplierRepository->findByName($this->request->supplierName)->willReturn($supplier);
        $orderFactory->createForShoppingCart($shoppingCart, $this->orderNumber, $this->recipient)->willReturn($order)->willThrow(InvalidArgumentException::class);

        $this->handle($this->request);

        $notificationService->notifyAbout(Argument::any(), Argument::any())->shouldNotHaveBeenCalled();
    }

    function it_does_not_notify_the_supplier_about_the_order_given_order_already_persisted(ShoppingCartRepository $shoppingCartRepository, ShoppingCart $shoppingCart, NotificationService $notificationService, OrderRepository $orderRepository, OrderFactory $orderFactory, Order $order, SupplierRepository $supplierRepository, Supplier $supplier)
    {
        $shoppingCartRepository->findByNumber($this->request->shoppingCartNumber)->willReturn($shoppingCart);
        $supplierRepository->findByName($this->request->supplierName)->willReturn($supplier);
        $orderFactory->createForShoppingCart($shoppingCart, $this->orderNumber, $this->recipient)->willReturn($order);
        $orderRepository->persist($order)->willThrow(AlreadyPersistedOrderException::class);

        $this->handle($this->request);

        $notificationService->notifyAbout(Argument::any(), Argument::any())->shouldNotHaveBeenCalled();
    }

    function it_returns_a_successful_response_given_the_shopping_cart_was_checked_out(ShoppingCartRepository $shoppingCartRepository, ShoppingCart $shoppingCart, OrderFactory $orderFactory, OrderRepository $orderRepository, Order $order, SupplierRepository $supplierRepository, Supplier $supplier)
    {
        $shoppingCartRepository->findByNumber($this->request->shoppingCartNumber)->willReturn($shoppingCart);
        $supplierRepository->findByName($this->request->supplierName)->willReturn($supplier);
        $orderFactory->createForShoppingCart($shoppingCart, $this->orderNumber, $this->recipient)->willReturn($order);
        $shoppingCartRepository->remove($shoppingCart)->shouldBeCalled();
        $orderRepository->persist($order)->shouldBeCalled();

        $response = $this->handle($this->request);

        $response->success->shouldBe(true);
    }

    function it_returns_an_unsuccessful_response_given_the_shopping_cart_was_not_checked_out(ShoppingCartRepository $shoppingCartRepository)
    {
        $shoppingCartRepository->findByNumber($this->request->shoppingCartNumber)->willThrow(CouldNotFindShoppingCartException::class);

        $response = $this->handle($this->request);

        $response->success->shouldBe(false);
    }
}
