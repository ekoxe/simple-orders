<?php

namespace spec\Ekoxe\SimpleOrders\Application\ShoppingCarts;

use Ekoxe\SimpleOrders\Application\ShoppingCarts\CloseShoppingCartRequest;
use Ekoxe\SimpleOrders\Domain\ShoppingCarts\CouldNotFindShoppingCartException;
use Ekoxe\SimpleOrders\Domain\ShoppingCarts\ShoppingCart;
use Ekoxe\SimpleOrders\Domain\ShoppingCarts\ShoppingCartNumber;
use Ekoxe\SimpleOrders\Domain\ShoppingCarts\ShoppingCartRepository;
use PhpSpec\ObjectBehavior;
use Prophecy\Argument;

class CloseShoppingCartSpec extends ObjectBehavior
{
    const A_SHOPPING_CART_NUMBER = 47;

    function let(ShoppingCartRepository $shoppingCartRepository)
    {
        $this->beConstructedWith($shoppingCartRepository);
    }

    function it_closes_a_shopping_cart(ShoppingCartRepository $shoppingCartRepository, ShoppingCart $shoppingCart)
    {
        $request = new CloseShoppingCartRequest(self::A_SHOPPING_CART_NUMBER);
        $shoppingCartRepository->findByNumber(new ShoppingCartNumber($request->shoppingCartNumber))->willReturn($shoppingCart);

        $shoppingCartRepository->remove($shoppingCart)->shouldBeCalled();

        $this->handle($request);
    }

    function it_returns_a_successful_response_given_shopping_cart_was_closed(ShoppingCartRepository $shoppingCartRepository, ShoppingCart $shoppingCart)
    {
        $request = new CloseShoppingCartRequest(self::A_SHOPPING_CART_NUMBER);
        $shoppingCartRepository->findByNumber(new ShoppingCartNumber($request->shoppingCartNumber))->willReturn($shoppingCart);
        $shoppingCartRepository->remove($shoppingCart)->shouldBeCalled();

        $response = $this->handle($request);

        $response->success->shouldBe(true);
    }

    function it_returns_an_successful_response_given_shopping_cart_was_not_closed(ShoppingCartRepository $shoppingCartRepository)
    {
        $request = new CloseShoppingCartRequest(self::A_SHOPPING_CART_NUMBER);
        $shoppingCartRepository->findByNumber(new ShoppingCartNumber($request->shoppingCartNumber))->willThrow(CouldNotFindShoppingCartException::class);

        $response = $this->handle($request);

        $response->success->shouldBe(false);
    }
}
