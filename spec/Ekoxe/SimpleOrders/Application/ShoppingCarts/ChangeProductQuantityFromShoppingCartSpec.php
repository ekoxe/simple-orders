<?php

namespace spec\Ekoxe\SimpleOrders\Application\ShoppingCarts;

use Ekoxe\SimpleOrders\Application\ShoppingCarts\ChangeProductQuantityFromShoppingCartRequest;
use Ekoxe\SimpleOrders\Domain\ShoppingCarts\CouldNotFindProductException;
use Ekoxe\SimpleOrders\Domain\ShoppingCarts\CouldNotFindShoppingCartException;
use Ekoxe\SimpleOrders\Domain\ShoppingCarts\ProductId;
use Ekoxe\SimpleOrders\Domain\ShoppingCarts\ProductReference;
use Ekoxe\SimpleOrders\Domain\ShoppingCarts\ShoppingCart;
use Ekoxe\SimpleOrders\Domain\ShoppingCarts\ShoppingCartNumber;
use Ekoxe\SimpleOrders\Domain\ShoppingCarts\ShoppingCartRepository;
use PhpSpec\ObjectBehavior;
use Prophecy\Argument;

class ChangeProductQuantityFromShoppingCartSpec extends ObjectBehavior
{
    const A_SHOPPING_CART_NUMBER = 4;
    const A_PRODUCT_REFERENCE = 'HAH';
    const A_QUANTITY = 7;
    const A_PRODUCT_NAME = 'doritos';

    private $productId;

    function let(ShoppingCartRepository $shoppingCartRepository)
    {
        $this->beConstructedWith($shoppingCartRepository);
        $this->productId = new ProductId(new ProductReference(self::A_PRODUCT_REFERENCE), self::A_PRODUCT_NAME);
    }

    function it_changes_product_quantity_from_shopping_cart(ShoppingCartRepository $shoppingCartRepository, ShoppingCart $shoppingCart)
    {
        $request = $this->prepareShoppingCartWithProduct($shoppingCartRepository, $shoppingCart);

        $this->handle($request);

        $shoppingCart->changeQuantityOf($this->productId, $request->quantity)->shouldHaveBeenCalled();
        $shoppingCartRepository->update($shoppingCart)->shouldHaveBeenCalled();
    }

    private function prepareShoppingCartWithProduct(ShoppingCartRepository $shoppingCartRepository, ShoppingCart $shoppingCart)
    {
        $request = new ChangeProductQuantityFromShoppingCartRequest(self::A_SHOPPING_CART_NUMBER, self::A_PRODUCT_REFERENCE, self::A_PRODUCT_NAME, self::A_QUANTITY);
        $shoppingCartRepository->findByNumber(new ShoppingCartNumber($request->shoppingCartNumber))->willReturn($shoppingCart);
        $shoppingCartRepository->update($shoppingCart)->shouldBeCalled();
        return $request;
    }

    function it_does_not_change_product_quantity_from_shopping_cart_given_nonexistent_shopping_cart(ShoppingCartRepository $shoppingCartRepository, ShoppingCart $shoppingCart)
    {
        $request = $this->prepareNonexistentShoppingCart($shoppingCartRepository);

        $this->handle($request);

        $shoppingCart->changeQuantityOf($this->productId, self::A_QUANTITY)->shouldNotHaveBeenCalled();
        $shoppingCartRepository->update($shoppingCart)->shouldNotHaveBeenCalled();
    }

    private function prepareNonexistentShoppingCart(ShoppingCartRepository $shoppingCartRepository)
    {
        $request = new ChangeProductQuantityFromShoppingCartRequest(self::A_SHOPPING_CART_NUMBER, self::A_PRODUCT_REFERENCE, self::A_PRODUCT_NAME, self::A_QUANTITY);
        $shoppingCartRepository->findByNumber(new ShoppingCartNumber(self::A_SHOPPING_CART_NUMBER))->willThrow(CouldNotFindShoppingCartException::class);
        return $request;
    }

    function it_does_not_change_product_quantity_from_shopping_cart_given_shopping_cart_does_not_contain_product(ShoppingCartRepository $shoppingCartRepository, ShoppingCart $shoppingCart)
    {
        $request = $this->prepareShoppingCartWithoutProduct($shoppingCartRepository, $shoppingCart);

        $this->handle($request);

        $shoppingCartRepository->update($shoppingCart)->shouldNotHaveBeenCalled();
    }

    private function prepareShoppingCartWithoutProduct(ShoppingCartRepository $shoppingCartRepository, ShoppingCart $shoppingCart)
    {
        $request = new ChangeProductQuantityFromShoppingCartRequest(self::A_SHOPPING_CART_NUMBER, self::A_PRODUCT_REFERENCE, self::A_PRODUCT_NAME, self::A_QUANTITY);
        $shoppingCartRepository->findByNumber(new ShoppingCartNumber($request->shoppingCartNumber))->willReturn($shoppingCart);
        $shoppingCart->changeQuantityOf($this->productId, $request->quantity)->willThrow(CouldNotFindProductException::class);
        return $request;
    }

    function it_returns_a_successful_response_given_product_quantity_was_changed(ShoppingCartRepository $shoppingCartRepository, ShoppingCart $shoppingCart)
    {
        $request = $this->prepareShoppingCartWithProduct($shoppingCartRepository, $shoppingCart);

        $response = $this->handle($request);

        $response->success->shouldBe(true);
    }

    function it_returns_an_unsuccessful_response_given_product_quantity_was_not_changed(ShoppingCartRepository $shoppingCartRepository, ShoppingCart $shoppingCart)
    {
        $request = $this->prepareShoppingCartWithoutProduct($shoppingCartRepository, $shoppingCart);

        $response = $this->handle($request);

        $response->success->shouldBe(false);
    }
}
