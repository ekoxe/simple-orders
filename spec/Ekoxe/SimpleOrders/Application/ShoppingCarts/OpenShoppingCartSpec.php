<?php

namespace spec\Ekoxe\SimpleOrders\Application\ShoppingCarts;

use Ekoxe\SimpleOrders\Application\ShoppingCarts\OpenShoppingCartRequest;
use Ekoxe\SimpleOrders\Application\ShoppingCarts\OpenShoppingCartResponse;
use Ekoxe\SimpleOrders\Domain\ShoppingCarts\CouldNotFindShoppingCartException;
use Ekoxe\SimpleOrders\Domain\ShoppingCarts\ShoppingCart;
use Ekoxe\SimpleOrders\Domain\ShoppingCarts\ShoppingCartFactory;
use Ekoxe\SimpleOrders\Domain\ShoppingCarts\ShoppingCartRepository;
use PhpSpec\ObjectBehavior;
use Prophecy\Argument;

class OpenShoppingCartSpec extends ObjectBehavior
{
    const A_CUSTOMER = 'awesome customer';

    function let(ShoppingCartRepository $shoppingCartRepository, ShoppingCartFactory $shoppingCartFactory)
    {
        $this->beConstructedWith($shoppingCartRepository, $shoppingCartFactory);
    }

    function it_opens_shopping_cart_of_customer(ShoppingCart $shoppingCart, ShoppingCartRepository $shoppingCartRepository)
    {
        $request = new OpenShoppingCartRequest(self::A_CUSTOMER);
        $shoppingCartRepository->findByCustomer(self::A_CUSTOMER)->shouldBeCalled()->willReturn($shoppingCart);

        $response = $this->handle($request);

        $expectedResponse = new OpenShoppingCartResponse($shoppingCart->getWrappedObject());
        $response->shouldBeLike($expectedResponse);
    }

    function it_opens_a_new_shopping_cart_given_customer_has_no_shopping_cart(ShoppingCart $shoppingCart, ShoppingCartRepository $shoppingCartRepository, ShoppingCartFactory $shoppingCartFactory)
    {
        $request = new OpenShoppingCartRequest(self::A_CUSTOMER);
        $shoppingCartRepository->findByCustomer($request->customer)->willThrow(CouldNotFindShoppingCartException::class);
        $shoppingCartFactory->create($request->customer)->shouldBeCalled()->willReturn($shoppingCart);
        $shoppingCartRepository->persist($shoppingCart)->shouldBeCalled();

        $response = $this->handle($request);

        $expectedResponse = new OpenShoppingCartResponse($shoppingCart->getWrappedObject());
        $response->shouldBeLike($expectedResponse);
    }
}
