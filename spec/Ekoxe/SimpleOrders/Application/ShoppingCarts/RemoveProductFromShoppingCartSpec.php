<?php

namespace spec\Ekoxe\SimpleOrders\Application\ShoppingCarts;

use Ekoxe\SimpleOrders\Application\ShoppingCarts\RemoveProductFromShoppingCartRequest;
use Ekoxe\SimpleOrders\Domain\ShoppingCarts\CouldNotFindProductException;
use Ekoxe\SimpleOrders\Domain\ShoppingCarts\CouldNotFindShoppingCartException;
use Ekoxe\SimpleOrders\Domain\ShoppingCarts\ProductId;
use Ekoxe\SimpleOrders\Domain\ShoppingCarts\ProductReference;
use Ekoxe\SimpleOrders\Domain\ShoppingCarts\ShoppingCart;
use Ekoxe\SimpleOrders\Domain\ShoppingCarts\ShoppingCartNumber;
use Ekoxe\SimpleOrders\Domain\ShoppingCarts\ShoppingCartRepository;
use PhpSpec\ObjectBehavior;
use Prophecy\Argument;

class RemoveProductFromShoppingCartSpec extends ObjectBehavior
{
    const A_SHOPPING_CART_NUMBER = 14;
    const A_PRODUCT_REFERENCE = 63;
    const A_PRODUCT_NAME = 'chair';

    private $productId;

    function let(ShoppingCartRepository $shoppingCartRepository)
    {
        $this->beConstructedWith($shoppingCartRepository);
        $this->productId = new ProductId(new ProductReference(self::A_PRODUCT_REFERENCE), self::A_PRODUCT_NAME);
    }

    function it_removes_product_from_shopping_cart(ShoppingCart $shoppingCart, ShoppingCartRepository $shoppingCartRepository)
    {
        $request = $this->prepareExistingShoppingCartWithProduct($shoppingCart, $shoppingCartRepository);

        $this->handle($request);

        $shoppingCart->remove($this->productId)->shouldHaveBeenCalled();
        $shoppingCartRepository->update($shoppingCart)->shouldHaveBeenCalled();
    }

    private function prepareExistingShoppingCartWithProduct(ShoppingCart $shoppingCart, ShoppingCartRepository $shoppingCartRepository)
    {
        $request = new RemoveProductFromShoppingCartRequest(self::A_SHOPPING_CART_NUMBER, self::A_PRODUCT_REFERENCE, self::A_PRODUCT_NAME);
        $shoppingCartRepository->findByNumber(new ShoppingCartNumber(self::A_SHOPPING_CART_NUMBER))->willReturn($shoppingCart);
        $shoppingCartRepository->update($shoppingCart)->shouldBeCalled();
        return $request;
    }

    function it_does_not_remove_product_given_shopping_cart_does_not_contain_product(ShoppingCart $shoppingCart, ShoppingCartRepository $shoppingCartRepository)
    {
        $request = $this->prepareExistingShoppingCartWithoutProduct($shoppingCart, $shoppingCartRepository);

        $this->handle($request);

        $shoppingCartRepository->update($shoppingCart)->shouldNotHaveBeenCalled();
    }

    private function prepareExistingShoppingCartWithoutProduct(ShoppingCart $shoppingCart, ShoppingCartRepository $shoppingCartRepository)
    {
        $request = new RemoveProductFromShoppingCartRequest(self::A_SHOPPING_CART_NUMBER, self::A_PRODUCT_REFERENCE, self::A_PRODUCT_NAME);
        $shoppingCartRepository->findByNumber(new ShoppingCartNumber($request->shoppingCartNumber))->willReturn($shoppingCart);
        $shoppingCart->remove($this->productId)->willThrow(CouldNotFindProductException::class);
        return $request;
    }

    function it_does_not_remove_product_given_nonexistent_shopping_cart(ShoppingCart $shoppingCart, ShoppingCartRepository $shoppingCartRepository)
    {
        $request = $this->prepareNonexistentShoppingCart($shoppingCartRepository);

        $this->handle($request);

        $shoppingCart->remove(Argument::any())->shouldNotHaveBeenCalled();
        $shoppingCartRepository->update(Argument::any())->shouldNotHaveBeenCalled();
    }

    private function prepareNonexistentShoppingCart(ShoppingCartRepository $shoppingCartRepository)
    {
        $request = new RemoveProductFromShoppingCartRequest(self::A_SHOPPING_CART_NUMBER, self::A_PRODUCT_REFERENCE, self::A_PRODUCT_NAME);
        $shoppingCartRepository->findByNumber(new ShoppingCartNumber($request->shoppingCartNumber))->willThrow(CouldNotFindShoppingCartException::class);
        return $request;
    }

    function it_returns_a_successful_response_given_the_product_was_removed_from_shopping_cart(ShoppingCart $shoppingCart, ShoppingCartRepository $shoppingCartRepository)
    {
        $request = $this->prepareExistingShoppingCartWithProduct($shoppingCart, $shoppingCartRepository);

        $response = $this->handle($request);

        $response->success->shouldBe(true);
    }

    function it_returns_an_unsuccessful_response_given_the_product_was_not_removed_from_shopping_cart(ShoppingCart $shoppingCart, ShoppingCartRepository $shoppingCartRepository)
    {
        $request = $this->prepareNonexistentShoppingCart($shoppingCartRepository);

        $response = $this->handle($request);

        $response->success->shouldBe(false);
    }
}
