<?php

namespace spec\Ekoxe\SimpleOrders\Application\ShoppingCarts;

use Ekoxe\SimpleOrders\Application\ShoppingCarts\AddProductIntoShoppingCartRequest;
use Ekoxe\SimpleOrders\Domain\ShoppingCarts\CouldNotFindShoppingCartException;
use Ekoxe\SimpleOrders\Domain\ShoppingCarts\Product;
use Ekoxe\SimpleOrders\Domain\ShoppingCarts\ProductFactory;
use Ekoxe\SimpleOrders\Domain\ShoppingCarts\ShoppingCart;
use Ekoxe\SimpleOrders\Domain\ShoppingCarts\ShoppingCartNumber;
use Ekoxe\SimpleOrders\Domain\ShoppingCarts\ShoppingCartRepository;
use PhpSpec\ObjectBehavior;
use Prophecy\Argument;

class AddProductIntoShoppingCartSpec extends ObjectBehavior
{
    const A_SHOPPING_CART_NUMBER = 1234;
    const A_PRODUCT_REFERENCE = 'T-BU660';
    const A_PRODUCT_NAME = 'Aspiration buccale';
    const A_QUANTITY = 4;

    function let(ShoppingCartRepository $shoppingCartRepository, ProductFactory $productFactory)
    {
        $this->beConstructedWith($shoppingCartRepository, $productFactory);
    }

    function it_adds_product_into_shopping_cart(ShoppingCart $shoppingCart, ShoppingCartRepository $shoppingCartRepository, Product $product, ProductFactory $productFactory)
    {
        $request = $this->prepareExistingShoppingCart($shoppingCart, $shoppingCartRepository, $product, $productFactory);

        $this->handle($request);

        $shoppingCart->add($product)->shouldHaveBeenCalled();
        $shoppingCartRepository->update($shoppingCart)->shouldHaveBeenCalled();
    }

    private function prepareExistingShoppingCart(ShoppingCart $shoppingCart, ShoppingCartRepository $shoppingCartRepository, Product $product, ProductFactory $productFactory)
    {
        $request = new AddProductIntoShoppingCartRequest(self::A_SHOPPING_CART_NUMBER, self::A_PRODUCT_REFERENCE, self::A_PRODUCT_NAME, self::A_QUANTITY);
        $shoppingCartRepository->findByNumber(new ShoppingCartNumber(self::A_SHOPPING_CART_NUMBER))->willReturn($shoppingCart);
        $productFactory->create(self::A_PRODUCT_REFERENCE, self::A_PRODUCT_NAME, self::A_QUANTITY)->willReturn($product);
        $shoppingCartRepository->update($shoppingCart)->shouldBeCalled();
        return $request;
    }

    function it_does_not_add_product_into_shopping_cart_given_nonexistent_shopping_cart(ShoppingCart $shoppingCart, ShoppingCartRepository $shoppingCartRepository, Product $product, ProductFactory $productFactory)
    {
        $request = $this->prepareNonexistentShoppingCart($shoppingCartRepository, $product, $productFactory);

        $this->handle($request);

        $shoppingCart->add($product)->shouldNotHaveBeenCalled();
        $shoppingCartRepository->update($shoppingCart)->shouldNotHaveBeenCalled();
    }

    private function prepareNonexistentShoppingCart(ShoppingCartRepository $shoppingCartRepository, Product $product, ProductFactory $productFactory)
    {
        $request = new AddProductIntoShoppingCartRequest(self::A_SHOPPING_CART_NUMBER, self::A_PRODUCT_REFERENCE, self::A_PRODUCT_NAME, self::A_QUANTITY);
        $shoppingCartRepository->findByNumber(new ShoppingCartNumber(self::A_SHOPPING_CART_NUMBER))->willThrow(CouldNotFindShoppingCartException::class);
        $productFactory->create(self::A_PRODUCT_REFERENCE, self::A_PRODUCT_NAME, self::A_QUANTITY)->willReturn($product);
        return $request;
    }

    function it_returns_a_successful_response_given_the_product_was_added_into_shopping_cart(ShoppingCart $shoppingCart, ShoppingCartRepository $shoppingCartRepository, Product $product, ProductFactory $productFactory)
    {
        $request = $this->prepareExistingShoppingCart($shoppingCart, $shoppingCartRepository, $product, $productFactory);

        $response = $this->handle($request);

        $response->success->shouldBe(true);
    }

    function it_returns_an_unsuccessful_response_given_the_product_was_not_added_into_shopping_cart(ShoppingCartRepository $shoppingCartRepository, Product $product, ProductFactory $productFactory)
    {
        $request = $this->prepareNonexistentShoppingCart($shoppingCartRepository, $product, $productFactory);

        $response = $this->handle($request);

        $response->success->shouldBe(false);
    }
}
