<?php

namespace spec\Ekoxe\SimpleOrders\Application\Notifications;

use Doctrine\Instantiator\Exception\InvalidArgumentException;
use Ekoxe\SimpleOrders\Application\Notifications\NotificationDeliverer;
use Ekoxe\SimpleOrders\Domain\Customers\Customer;
use Ekoxe\SimpleOrders\Domain\Customers\CustomerId;
use Ekoxe\SimpleOrders\Domain\Orders\Order;
use Ekoxe\SimpleOrders\Domain\Orders\OrderNumber;
use Ekoxe\SimpleOrders\Domain\Orders\Recipient;
use Ekoxe\SimpleOrders\Domain\Suppliers\ContactInformation;
use Ekoxe\SimpleOrders\Domain\Suppliers\Supplier;
use PhpSpec\ObjectBehavior;

class NotificationServiceSpec extends ObjectBehavior
{
    const A_CUSTOMER = 'Tenausys';
    const A_RECIPIENT_NAME = 'Ian Letourneau';
    const A_RECIPIENT_EMAIL = 'info@swican.com';
    const A_SUPPLIER_NAME = 'Formamed Sarl';
    const A_SUPPLIER_EMAIL = 'commandes@formamed.ch';

    private $contactInformation;

    function let(NotificationDeliverer $notificationDeliverer)
    {
        $this->beConstructedWith($notificationDeliverer);
        $this->contactInformation = new ContactInformation(self::A_SUPPLIER_NAME, self::A_SUPPLIER_EMAIL);
    }

    function it_delivers_notification_to_supplier_about_order(NotificationDeliverer $notificationDeliverer)
    {
        $supplier = new Supplier($this->contactInformation);
        $order = $this->createOrder();

        $this->notifyAbout($supplier, $order);

        $notificationDeliverer->prepareNotification($supplier, $order)->shouldHaveBeenCalled();
        $notificationDeliverer->deliverNotification()->shouldHaveBeenCalled();
    }

    private function createOrder(): Order
    {
        $orderNumber = new OrderNumber(957);
        $customer = new Customer(new CustomerId(self::A_CUSTOMER));
        $recipient = new Recipient(self::A_RECIPIENT_NAME, self::A_RECIPIENT_EMAIL);
        return new Order($orderNumber, $customer, $recipient);
    }

    function it_does_not_deliver_notification_for_order_given_invalid_recipient(NotificationDeliverer $notificationDeliverer)
    {
        $supplier = new Supplier($this->contactInformation);
        $order = $this->createOrder();
        $notificationDeliverer->prepareNotification($supplier, $order)->willThrow(InvalidArgumentException::class);

        $notificationDeliverer->deliverNotification()->shouldNotBeCalled();

        $this->notifyAbout($supplier, $order);
    }
}
